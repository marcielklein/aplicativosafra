<?php
    require_once './protected/libs/controlador.php';
    include 'config/configdbrelatorio.php';
    session_start();
    if ($_SESSION['cpf'] == null) {
        header('location:acesso/login.php');
    }
    
    //Verifica o tipo do usuário
    $sqlconsultaverificausuario = pg_query("SELECT id, tipousuario, SUBSTRING(nome from 1 for 10) as nome FROM usuario u where u.cpf = " . "'" .  $_SESSION['cpf'] . "'");
    $resconsultaverificausuario   = pg_fetch_array ($sqlconsultaverificausuario);
    $tipousuario = $resconsultaverificausuario['tipousuario'];
    $idusuario = $resconsultaverificausuario['id'];
    $nomeusuario = $resconsultaverificausuario['nome'];
    
    //Post para atualizar a flag do ano tabela anosafra
    if(isset($_POST['ano']) != '' & isset($_POST['idcultura']) == '' & isset($_POST['idarea']) == ''){
        $cpf = $_SESSION['cpf'];
        $anosafraatualizaflag = $_POST['ano'];
        
        //Atualiza a flag ativo da tabela anosafra
        $sqlatualizaanosafra = pg_query("update anosafra set ativo = 'A' where idusuario = $idusuario and ano = $anosafraatualizaflag;");
        if (!$sqlatualizaanosafra){
            echo "<div id='avisoAnoDefinido' style='display: none; color: red; text-align: center'><b>Não foi possível definir o Ano :(</b></div>";
        }else{
            //Consulta se existe alguma flag já setada como Ativa
            $consultaflagativa = pg_query("select ativo from anosafra where idusuario = $idusuario and ativo = 'A'");
            $resconsultaflagativa = pg_fetch_array ($consultaflagativa);
            $flagativa = $resconsultaflagativa['ativo'];
            if($flagativa != ''){
                $sqlatualizaflagativa = pg_query("update anosafra set ativo = '' where idusuario = $idusuario;");
                if(!$sqlatualizaflagativa){
                    echo "<div id='avisoAnoDefinido' style='display: none; color: red; text-align: center'><b>Não foi possível definir o Ano :(</b></div>";
                }else{
                    $sqlatualizaanosafra = pg_query("update anosafra set ativo = 'A' where idusuario = $idusuario and ano = $anosafraatualizaflag;");
                    echo "<div id='avisoAnoDefinido' style='display: none; color: blue; text-align: center'><b>Ano definido com sucesso!</b></div>";
                }
            }
        }
        echo "<div id='avisoAnoDefinido' style='display: none; color: red; text-align: center'><b>Não foi possível definir o Ano :( <br/> Pois não existe nenhuma Propriedade cadastrada.</b></div>";
    }
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Aplicativo Safra</title>
        <link href="includes/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="includes/css/datatables.min.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="includes/css/bootstrap-select.min.css">

        <link rel="stylesheet" href="includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maskMoney.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="includes/js/validarcampos.js" type="text/javascript"></script>
        <script src="includes/js/vanilla-masker.min.js" type="text/javascript"></script>
        <script src="includes/js/bootstrap-select.min.js" type="text/javascript"></script>

        <!-- Excluir Registro - Mensagem-->
        <script src="includes/js/jquery-1.11.3.js"></script>
        <script src="includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="includes/css/jquery-ui.css" type="text/css" />
        
        <!-- JQuery formata Valores -->
        <script type="text/javascript">
            $(function(){
                $("#avisoAnoDefinido").fadeIn(3000, function(){
                    window.setTimeout(function(){
                        $('#avisoAnoDefinido').fadeOut();
                    }, 5000);
                });
            });
        </script>

        <!-- Abrir Pop-Up relatório-->
        <script language="JavaScript">
            
            function abrir(URL) {
                var width = 500;
                var height = 250;
                var left = 380;
                var top = 200;
                window.open(URL, 'janela', 'width=' + width +
                        ', height=' + height +
                        ', top=' + top +
                        ', left=' + left +
                        ', scrollbars=yes, \n\
                                 status=no, \n\
                                 toolbar=no, \n\
                                 location=no, \n\
                                 directories=no, \n\
                                 menubar=no, \n\
                                 resizable=no, \n\
                                 fullscreen=no');

            }
        </script>
    </head>

    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #006400; margin-bottom: 0px">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menutitle"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="width: 1200px;">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php" title="Clique aqui para ir para o início do painel" id="menutitle">Home</a></li>
                        <li><a href="index.php?controle=propriedadeController&acao=listar" title="Clique aqui para visualizar as Propriedades" id="menutitle">Cadastro de Propriedade</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" id="menutitle" aria-expanded="true">Cadastros<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                    <li><a href="index.php?controle=culturaController&amp;acao=listar">Cultura</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="index.php?controle=plantioController&amp;acao=listar">Plantio</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="index.php?controle=colheitaController&amp;acao=listar">Colheita</a></li>
                                    <?php
                                    if($tipousuario == 'A'){?>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="index.php?controle=usuarioController&acao=listar">Usuário</a></li>
                                    <?php }
                            ?>
                            </ul>
                        </li>
                        
                        <li><a href="index.php?controle=relacaogastosController&acao=listar" title="Clique aqui para visualizar os Gastos" id="menutitle">Relação de Gastos Propriedade</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" id="menutitle" aria-expanded="true">Relatórios<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:abrir('protected/view/relatorio_propriedade/filtro_rel_propriedade.php');">Relatório por Propriedade</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:abrir('protected/view/relatorio_area/filtro_rel_area.php');">Relatório por Área</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:abrir('protected/view/relatorio_cultura/filtro_rel_cultura.php');">Relatório por Cultura</a></li>
                            </ul>
                        </li>
                        <li><a href="/aplicativosafra/acesso/logout.php" id="menutitle" style="padding-right: 50px;" title="Clique aqui para sair do sistema">Sair</a></li>
                        <?php
                            $sqlconsultaanosafrausuario = pg_query("SELECT a.ano, a.idusuario FROM anosafra a left join usuario u on u.id = a.idusuario where u.cpf = " . "'" .  $_SESSION['cpf'] . "'" . " order by a.id asc;");
                            $resanosafrausuario   = pg_fetch_array ($sqlconsultaanosafrausuario);
                            $anosafrausuario = $resanosafrausuario['ano'];
                            if($anosafrausuario == ""){
                                //Busca o ano automaticamente
				$anosafrausuario = date('Y');
                            }
                        ?>
                        <li>
                            <form method="post" action="index.php">
                                <div class="row">
                                    <div class="col-md-6" style="padding-right: 0px;">
                                        <select class="form-control" name="ano" id="ano" required data-errormessage-value-missing="Selecione o Ano" 
                                        style="border-top-width: 1px; margin-top: 10px;">
                                            <?php
                                                $buscaranosafra = date('Y');
                                                //Verificar se o buscarano possui propriedade, se não possuir deve aparecer como disabled
                                                $queryano = pg_query("SELECT count(*) as existe FROM anosafra a left join usuario u on u.id = a.idusuario where u.cpf = " . "'" .  $_SESSION['cpf'] . "'" . " and a.ano = $buscaranosafra;");
                                                $resconsulta = pg_fetch_array($queryano);
                                                $existeanosafra = $resconsulta['existe'];
                                                
                                                if($existeanosafra == 0){
                                                    $class_disabled = "disabled";
                                                    $title_no_propriedade = "title= 'Não existe propriedade cadastrada para este ano!'";
                                                }else{
                                                    $class_disabled = "";
                                                    $title_no_propriedade = "";
                                                }
                                                    
                                                if($anosafrausuario != ''){
                                                    $query = "SELECT distinct a.ano, a.ativo FROM anosafra a left join usuario u on u.id = a.idusuario where u.cpf = " . "'" .  $_SESSION['cpf'] . "'" . " order by a.ativo desc;";
                                                    $resultexiste = pg_query($query);
                                                    $anoexiste = $resultexiste['ano'];
                                                    
                                                    $quantidadelinhaanosafra = pg_num_rows($resultexiste);
                                                    if($quantidadelinhaanosafra == 0){
                                                        $buscaranosafra = date('Y');
                                                        ?>
                                                        <option value="<?php echo $buscaranosafra?>"><?php echo $buscaranosafra; ?></option>
                                                    <?php }else{
                                                        if($title_no_propriedade != ""){ ?>
                                                        <option selected="" value="<?php echo $buscaranosafra?>" <?php echo $title_no_propriedade; ?> <?php echo $class_disabled?>><?php echo $buscaranosafra; ?></option>
                                                        <?php }
                                                        ?>
                                                        <?php while ($consultaexiste = pg_fetch_assoc($resultexiste)) {
                                                            $anosafrausuarioexiste = $consultaexiste['ano'];
                                                            ?>
                                                            <option value="<?php echo $anosafrausuarioexiste?>"><?php echo $anosafrausuarioexiste; ?></option>
                                                        <?php }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2" style="padding-left: 0px;">
                                            <button type="submit" title="Clique aqui para definir o ano" class="btn btn-success" style="margin-top: 10px; margin-left: 8px;">Definir Ano</button>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li id="menutitle" style="margin-left: 30px; font-size: 12px; margin-top: 4px;"><b>Logado como: </b> <?php echo $nomeusuario;?></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <div class="container" id="fundo">
            <?php
                $app = new Controlador();
            ?>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="includes/js/bootstrap.min.js"></script>

        <!-- DataTables -->
        <script src="includes/js/jquery.dataTables.min.js"></script>
        <script src="includes/js/dataTables.bootstrap.min.js"></script>
        <script>
            $(function () {
                $("#example1").DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "oLanguage": {
                        "sProcessing": "Aguarde enquanto os dados são carregados ...",
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                        "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                        "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                        "sInfoFiltered": "",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sFirst": "Primeiro",
                            "sPrevious": "Anterior",
                            "sNext": "Próximo",
                            "sLast": "Último"
                        }
                    }
                });
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "oLanguage": {
                        "sProcessing": "Aguarde enquanto os dados são carregados ...",
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                        "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                        "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                        "sInfoFiltered": "",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sFirst": "Primeiro",
                            "sPrevious": "Anterior",
                            "sNext": "Próximo",
                            "sLast": "Último"
                        }
                    }
                });
            });
        </script>
    </body>
</html>

