<?php

class ProdutoController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/produtoModel.php';
        $this->model = new ProdutoModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=produtoController&acao=inserir';
        require './protected/view/produto/formProduto.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r == 1){
            //se entrou aqui quer dizer que o registro foi gravado
            //consulta qual é o id através do post
            $id = $_POST['idarea'];
            //chama a listagem de área
            $listaareaproduto = $this->model->areaproduto($id);
        }
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        require './protected/view/produto/listProduto.php';
    }
    
    public function areaproduto($id) {
        $listaareaproduto = $this->model->areaproduto($id);
        require './protected/view/produto/listProduto.php';
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/produto/listProduto.php';
    }
    
    public function buscar($id) {
        $produto = $this->model->buscar($id);
        $acao = 'index.php?controle=produtoController&acao=atualizar';
        require './protected/view/produto/formProduto.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r == 1){
            //se entrou aqui quer dizer que o registro foi gravado
            //consulta qual é o id através do post
            $id = $_POST['idarea'];
            $consultaareaproduto = pg_query("select pro.idarea
                                               from produto pro
                                              inner join area a
                                                 on pro.idarea = a.id
                                              where pro.id = $id");
            $resconsultaareaproduto   = pg_fetch_array ($consultaareaproduto);
            $idarea = $resconsultaareaproduto['idarea'];
            //chama a listagem de área
            $listaareaproduto = $this->model->areaproduto($idarea);
        }
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        require './protected/view/produto/listProduto.php';
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r == 1){
            //se entrou aqui quer dizer que o registro foi gravado
            //consulta qual é o id através do post
            $id = $_GET['idarea'];
            //chama a listagem de área
            $listaareaproduto = $this->model->areaproduto($id);
        }
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Produto possui registros filhos.
                  </div>';
        }
        require './protected/view/produto/listProduto.php';
    }
}