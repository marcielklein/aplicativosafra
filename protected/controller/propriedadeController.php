<?php

class PropriedadeController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/propriedadeModel.php';
        $this->model = new PropriedadeModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=propriedadeController&acao=inserir';
        require './protected/view/propriedade/formPropriedade.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível inserir a propriedade, pois o código da propriedade já existe para este usuário.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/propriedade/listPropriedade.php';
    }
    
    public function buscar($id) {
        $propriedade = $this->model->buscar($id);
        $acao = 'index.php?controle=propriedadeController&acao=atualizar';
        require './protected/view/propriedade/formPropriedade.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não é possível atualizar a propriedade, pois o código da propriedade já existe para este usuário.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Propriedade pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}