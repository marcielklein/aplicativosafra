<?php

class RelacaogastosController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/relacaogastosModel.php';
        $this->model = new RelacaogastosModel();
    }
    
    public function listar() {
        require './protected/view/relacaogastos/listRelacaogastos.php';
    }
}