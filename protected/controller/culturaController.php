<?php

class CulturaController {
    private $bd, $model;
    private $areaModel;
    
    function __construct() {
        require './protected/model/culturaModel.php';
        require './protected/model/areaModel.php';
        $this->model = new CulturaModel();
        $this->modelAreas = new AreaModel();
    }
    
    public function novo() {
        $listaAreas  = $this->modelAreas->buscarTodos();
        $acao = 'index.php?controle=culturaController&acao=inserir';
        require './protected/view/cultura/formCultura.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível inserir a cultura, pois esta cultura já encontra-se cadastrada.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/cultura/listCultura.php';
    }
    
    public function buscar($id) {
        $cultura = $this->model->buscar($id);
        $listaAreas  = $this->modelAreas->buscarTodos();
        $acao = 'index.php?controle=culturaController&acao=atualizar';
        require './protected/view/cultura/formCultura.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não é possível atualizar a cultura, esta cultura já encontra-se cadastrada.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a cultura pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}