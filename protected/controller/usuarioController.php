<?php

class UsuarioController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/usuarioModel.php';
        $this->model = new UsuarioModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=usuarioController&acao=inserir';
        require './protected/view/usuario/formUsuario.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/usuario/listUsuario.php';
    }
    
    public function buscar($id) {
        $usuario = $this->model->buscar($id);
        $acao = 'index.php?controle=usuarioController&acao=atualizar';
        require './protected/view/usuario/formUsuario.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Usuário pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
    
    //Função para autorizar o usuário
    public function autorizar($id){
        $r = $this->model->autorizar($id);
        if($r){
            echo '<div class="alert alert-success">
                    Acesso do usuário alterado com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível alterar o acesso do Usuário.
                  </div>';
        }
        $this->listar();
    }
}