<?php

class ColheitaController {
    private $bd, $model;
    private $plantioModel;
    private $colheitaModel;
    
    function __construct() {
        require './protected/model/colheitaModel.php';
        require './protected/model/plantioModel.php';
        $this->model = new ColheitaModel();
        $this->modelPlantios = new PlantioModel();
    }
    
    public function novo() {
        $listaPlantios  = $this->modelPlantios->buscarTodos();
        $acao = 'index.php?controle=colheitaController&acao=inserir';
        require './protected/view/colheita/formColheita.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/colheita/listColheita.php';
    }
    
    public function buscar($id) {
        $colheita   = $this->model->buscar($id);
        $listaPlantios  = $this->modelPlantios->buscarTodos();
        $acao = 'index.php?controle=colheitaController&acao=atualizar';
        require './protected/view/colheita/formColheita.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                   Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Colheita pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}