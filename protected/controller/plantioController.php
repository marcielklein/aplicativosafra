<?php

class PlantioController {
    private $bd, $model;
    private $culturaModel;
    private $areaModel;
    private $plantioModel;
    
    function __construct() {
        require './protected/model/plantioModel.php';
        require './protected/model/culturaModel.php';
        require './protected/model/areaModel.php';
        $this->model = new PlantioModel();
        $this->modelCulturas = new CulturaModel();
        $this->modelAreas = new AreaModel();
    }
    
    public function novo() {
        $listaCulturas  = $this->modelCulturas->buscarTodos();
        $listaAreas  = $this->modelAreas->buscarTodos();
        $acao = 'index.php?controle=plantioController&acao=inserir';
        require './protected/view/plantio/formPlantio.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/plantio/listPlantio.php';
    }
    
    public function buscar($id) {
        $plantio   = $this->model->buscar($id);
        $listaCulturas  = $this->modelCulturas->buscarTodos();
        $listaAreas  = $this->modelAreas->buscarTodos();
        $acao = 'index.php?controle=plantioController&acao=atualizar';
        require './protected/view/plantio/formPlantio.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                   Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Plantio pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}