<?php

class AreaController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/areaModel.php';
        $this->model = new AreaModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=areaController&acao=inserir';
        require './protected/view/area/formArea.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r == 1){
            //consulta qual é o id através do post
            $id = $_POST['idpropriedade'];
            //chama a listagem de área
            $listaareapropriedade = $this->model->areapropriedade($id);
        }
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            //Redireciona para a Relação de Área
            $server_name = $_SERVER["SERVER_NAME"];
            $server_port = $_SERVER["SERVER_PORT"];
            echo "<script>window.location.href='http://". $server_name .":" . $server_port . "/aplicativosafra/index.php?controle=propriedadeController&acao=listar&erro=area'</script>";
        }
        require './protected/view/area/listArea.php';
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/area/listArea.php';
    }
    
    public function areapropriedade($id) {
        $listaareapropriedade = $this->model->areapropriedade($id);
        require './protected/view/area/listArea.php';
    }
    
    
    public function buscar($id) {
        $area = $this->model->buscar($id);
        $acao = 'index.php?controle=areaController&acao=atualizar';
        require './protected/view/area/formArea.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r== 0 || $r == 1){
            //consulta qual é o id através do post
            $id = $_POST['idpropriedade'];
            //chama a listagem de área
            $listaareapropriedade = $this->model->areapropriedade($id);
        }
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar o registro, pois o código da área já existe para este usuário.
                  </div>';
        }
        require './protected/view/area/listArea.php';
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r == 1){
            //Irá entrar aqui somente se o registro realmente for excluido
            //verifica qual é o id da propriedade para retornar as áreas somente que pertencem a propriedade
            $id = $_GET['idpropriedade'];
            $listaareapropriedade = $this->model->areapropriedade($id);
        }
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            $server_name = $_SERVER["SERVER_NAME"];
            $server_port = $_SERVER["SERVER_PORT"];
            echo "<script>window.location.href='http://". $server_name .":" . $server_port . "/aplicativosafra/index.php?controle=propriedadeController&acao=listar&erroarea=1'</script>";
        }
        require './protected/view/area/listArea.php';
    }
}