<?php

class CulturaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $descricaocultura = $_POST['descricao'];
        $idarea = $_POST['idarea'];
        
        //Buscar idusuario
        $sqlconsultaidusuario = pg_query("SELECT id as idusuario FROM usuario u where u.cpf = " . "'" . $_SESSION['cpf'] . "'");
        $resconsultaidusuario = pg_fetch_array($sqlconsultaidusuario);
        $idusuario = $resconsultaidusuario['idusuario'];
        
        //Buscar ano automático
        $buscar_ano = date('Y');
        
        $sql = "INSERT INTO cultura(descricao, anosafra, idusuario, idarea) "
                . " VALUES('$descricaocultura', $buscar_ano, $idusuario, $idarea)";
        unset($dados['id']);
        unset($dados['descricao']);
        unset($dados['idarea']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = " SELECT distinct cul.id as id,
                        cul.descricao as descricao,
                        are.descricao as descricaoarea,
                        pro.descricao as descricaopropriedade,
                        cul.anosafra
                   FROM cultura cul
                  INNER JOIN usuario usu
                     ON cul.idusuario = usu.id
                  INNER JOIN area are
                     ON cul.idarea = are.id
                  INNER JOIN propriedade pro
                     ON are.idpropriedade = pro.id
                  INNER JOIN anosafra ano
                     ON ano.ano = cul.anosafra
                  WHERE usu.cpf = " . "'" . $_SESSION['cpf'] . "'
                    AND ano.ativo = 'A'
                 ORDER BY descricaoarea, descricaopropriedade asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, descricao, idarea FROM cultura WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $descricaocultura = $_POST['descricao'];
        $idcultura = $_POST['id'];
        
        $sql = "UPDATE cultura SET descricao = :descricao, idarea = :idarea WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        //Remover a cultura
        $sql = "DELETE FROM cultura WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
