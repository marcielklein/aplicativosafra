<?php

class PropriedadeModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $codigopropriedade = $_POST['codigopropriedade'];
        
        //Verifica se o idpropriedade ja existe para o id do usuário na tabela anosafra
        $consulturapropriedadeusuario = pg_query("SELECT count(*) as existe 
                                                    FROM propriedade pro 
                                                   INNER JOIN anosafra a 
                                                      ON a.idpropriedade = pro.id
                                                   INNER JOIN usuario u
                                                      ON u.id = a.idusuario
                                                   WHERE u.cpf = " . "'" . $_SESSION['cpf'] . "'   
                                                     AND pro.codigopropriedade = $codigopropriedade");
        
        $resconsultacodigopropriedade = pg_fetch_array($consulturapropriedadeusuario);
        $codigopropriedade = $resconsultacodigopropriedade['existe'];
        if ($codigopropriedade == "0") {
            $sql = "INSERT INTO propriedade(codigopropriedade, descricao) "
                    . " VALUES(:codigopropriedade, :descricao)";
            unset($dados['id']);
            $query = $this->bd->prepare($sql);
            if ($query->execute($dados) == 1) {
                //Consultar o ultimo id da propriedade inserido
                $sqlconsultapropriedade = pg_query("SELECT max(id) as idpropriedade FROM propriedade");
                $resconsultapropriedade = pg_fetch_array($sqlconsultapropriedade);
                $idpropriedade = $resconsultapropriedade['idpropriedade'];
                
                //Buscar Ano da Safra
                $buscaranosafra = date('Y');
                
                //Buscar idusuario
                $sqlconsultaidusuario = pg_query("SELECT id as idusuario FROM usuario u where u.cpf = " . "'" . $_SESSION['cpf'] . "'");
                $resconsultaidusuario = pg_fetch_array($sqlconsultaidusuario);
                $idusuario = $resconsultaidusuario['idusuario'];

                //Insere na tabela anosafra
                $sqlinsertanosafra = "INSERT INTO anosafra(ano, idusuario, idpropriedade) VALUES($buscaranosafra, $idusuario, $idpropriedade)";
                unset($dados['id']);
                unset($dados['ano']);
                unset($dados['idusuario']);
                unset($dados['idpropriedade']);
                unset($dados['codigopropriedade']);
                unset($dados['descricao']);
                $queryanosafra = $this->bd->prepare($sqlinsertanosafra);
                
                if($queryanosafra->execute($dados) == 1){
                    #Se cair neste bloco quer dizer que inseriu o registro
                    #Então devemos atualizar a coluna Ativo da tabela AnoSafra
                    return $sqlatualizaanosafra = pg_query("UPDATE anosafra SET ativo = 'A' WHERE ano = $buscaranosafra and idusuario = $idusuario and idpropriedade = $idpropriedade");
                }
            }
        }
    }

    public function buscarTodos() {
        //Buscar Ano da Safra
        $sql = "SELECT pro.id as id,
                       pro.codigopropriedade as codigopropriedade,
                       pro.descricao as descricao,
                       safra.ano
                  FROM anosafra safra
                  LEFT JOIN propriedade pro
                    ON safra.idpropriedade = pro.id
                  LEFT JOIN usuario usu
                    ON safra.idusuario = usu.id
                 WHERE usu.cpf = " . "'" . $_SESSION['cpf'] . "' 
                   AND safra.ativo = 'A'
                 ORDER BY descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, codigopropriedade, descricao FROM propriedade WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $codigopropriedade = $_POST['codigopropriedade'];
        $idpropriedade = $_POST['id'];
        
        //Consulta o Código propriedade
        $consultapropriedadeverusuario = pg_query("SELECT count(*) as existe
                                                     FROM propriedade pro 
                                                    INNER JOIN anosafra a 
                                                       ON a.idpropriedade = pro.id
                                                    INNER JOIN usuario u
                                                       ON u.id = a.idusuario
                                                    WHERE u.cpf = " . "'" . $_SESSION['cpf'] . "'
                                                      AND pro.codigopropriedade = $codigopropriedade
                                                      AND pro.id <> $idpropriedade");
        $resconsultaverpropriedade = pg_fetch_array($consultapropriedadeverusuario);
        $codigoexiste = $resconsultaverpropriedade['existe'];
        
        if ($codigoexiste == "0") {
            $sql = "UPDATE propriedade SET codigopropriedade = :codigopropriedade,  descricao = :descricao WHERE id = :id";
            $query = $this->bd->prepare($sql);
            return $query->execute($dados);
        }
    }

    public function excluir($id) {
        //Remover o ano safra do usuário
        $sqlexcluianosafra = "DELETE FROM anosafra WHERE idpropriedade = :id";
        $query = $this->bd->prepare($sqlexcluianosafra);
        $query->execute(array('id' => $id));
        
        //Remover a propriedade do usuário
        $sql = "DELETE FROM propriedade WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
