<?php

class ColheitaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $sql = "INSERT INTO colheita(idplantio, peso, ano) "
                . " VALUES(:idplantio, :peso, :ano)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
    	$sql = "select  distinct col.id,
                       col.peso,
                       col.ano,
                       plan.id as idplantio,
                       (cul.descricao || ' - ' || ar.descricao) as descricaocultura,
                       (ar.codigoarea || ' - ' || ar.descricao) as descricaoarea
                  from colheita col
                 inner join plantio plan
                    on col.idplantio = plan.id
                 inner join cultura cul
                    on plan.idcultura = cul.id
                 inner join area ar
                    on plan.idarea = ar.id
                 inner join propriedade pro
                    on ar.idpropriedade = pro.id
                 inner join usuario usu
                    on cul.idusuario = usu.id
                 inner join anosafra ano
                    on ano.ano = col.ano 
                 where usu.cpf = " . "'" . $_SESSION['cpf'] . "'
                   and ano.ativo = 'A'
                 order by col.ano asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select col.id,
                       col.peso,
                       col.ano,
                       plan.id as idplantio,
                       cul.descricao as descricaocultura,
                       (ar.codigoarea || ' - ' || ar.descricao) as descricaoarea
                  from colheita col
                 inner join plantio plan
                    on col.idplantio = plan.id
                 inner join cultura cul
                    on plan.idcultura = cul.id
                 inner join area ar
                    on plan.idarea = ar.id
                 WHERE col.id = :id;";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $sql = "UPDATE colheita 
                SET idplantio = :idplantio, 
                    peso    = :peso, 
                    ano       = :ano
                WHERE 
                    id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM colheita WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }
}