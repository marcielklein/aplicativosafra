<?php

class PlantioModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $idcultura = $_POST['idcultura'];
        $idarea    = $_POST['idarea'];
        $ano       = $_POST['ano'];
        
        $sql = "INSERT INTO plantio(idcultura, idarea, ano) "
                . " VALUES($idcultura, $idarea, $ano)";
        unset($dados['id']);
        unset($dados['idcultura']);
        unset($dados['idarea']);
        unset($dados['ano']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "select distinct plan.id,
                       plan.ano,
                       (cul.descricao || ' - ' || ar.descricao) as descricaocultura,
                       (ar.codigoarea || ' - ' || ar.descricao) as descricaoarea 
                  from plantio plan
                 inner join cultura cul
                    on plan.idcultura = cul.id
                 inner join area ar
                    on plan.idarea = ar.id
                 inner join propriedade pro
                    on ar.idpropriedade = pro.id
                 inner join anosafra ano
                    on ano.ano = cul.anosafra
                 inner join usuario usu
                    on cul.idusuario = usu.id
                 where usu.cpf = " . "'" . $_SESSION['cpf'] . "'
                   and ano.ativo = 'A'
                 order by plan.ano asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT p.id,
                       p.idarea,
                       p.idcultura,
                       p.ano,
                       c.descricao as descricaocultura,
                       ar.descricao as descricaoarea
                  FROM plantio p
                 INNER JOIN cultura c
                    ON p.idcultura = c.id
                 INNER JOIN area ar
                    ON ar.id = p.idarea
                 WHERE p.id = :id;";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $sql = "UPDATE plantio 
                SET idcultura = :idcultura, 
                    idarea    = :idarea, 
                    ano       = :ano
                WHERE 
                    id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM plantio WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }
}