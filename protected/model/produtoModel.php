<?php

class ProdutoModel extends Conexao{
    
    function __construct() {
        parent::__construct();
    }
    
    public function inserir(array $dados) {
        $idarea = $_POST['idarea'];
        $descricao = $_POST['descricao'];
        $quantidade = $_POST['quantidade'];
        
        //Buscar o ano
        $consultaano = pg_query("SELECT idano, idpropriedade FROM area a where id = $idarea");
        $resconsultaano = pg_fetch_array($consultaano);
        $idano = $resconsultaano['idano'];
        $idpropriedade = $resconsultaano['idpropriedade'];
        
        $valor = $_POST['valor'];
        $formata = str_replace("R$", "", str_replace("", "", $valor));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        $valor = $formatavalor;
        
        $sql   = "INSERT INTO produto(descricao, valor, quantidade, idarea, idano, idpropriedade) "
                . " VALUES(:descricao, $valor, :quantidade, :idarea, $idano, $idpropriedade)";
        
        unset($dados['id']);
        unset($dados['valor']);
        unset($dados['idproduto']);
        unset($dados['descricaoarea']);
        unset($dados['descricaopropriedade']);
        unset($dados['idano']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function buscarTodos(){
        $anoautomatico = date('Y');
        $sql   = "SELECT prod.id as id, 
                         prod.descricao as descricao, 
                         prod.valor as valor, 
                         prod.quantidade as quantidade
                    FROM produto prod
                   INNER JOIN propriedade prop
                      ON prod.idpropriedade = prop.id 
                   INNER JOIN anosafra ano
                      ON ano.idpropriedade = prop.id
                   INNER JOIN usuario usu
                      ON ano.idusuario = usu.id
                   WHERE usu.cpf = " . "'" . $_SESSION['cpf'] . "'
                     AND ano.ano = $anoautomatico
                   ORDER BY prod.descricao ASC;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscar($id) {
        $sql = "SELECT id, descricao, valor, quantidade FROM produto "
                . " WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id'=>$id));
        
        return $query->fetch();
    }
    
    public function areaproduto($id){
        $sql = "SELECT pro.id,
                       pro.descricao as descricaoproduto,
                       to_char(pro.valor, 'L9G999G990D99') as valorproduto,
                       pro.quantidade as quantidadeproduto,
                       pro.idarea 
                  FROM produto pro
                 INNER JOIN area a
                    ON pro.idarea = a.id
                 WHERE pro.idarea = :id
              ORDER BY descricaoproduto asc; ";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id'=>$id));
        return $query->fetchAll();
    }

    public function atualizar(array $dados) {
        
        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valor']));
        
        //Valor formatação
        $valorproduto = $formata;
        $verificavalorproduto = substr($valorproduto, -3, 1);
        if($verificavalorproduto == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorproduto));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorprodutoformatado   = $formatavalor;
        }else{
            $valorproduto = $dados['valor'];
            $formata      = str_replace("R$", "", str_replace("", "", $valorproduto));
            $removevirgula = str_replace(",", "", $formata);
            $valorprodutoformatado = $removevirgula;
        }

        
        $sql = "UPDATE produto SET descricao = :descricao,  valor = $valorprodutoformatado, quantidade = :quantidade WHERE id = :idproduto";
        
        unset($dados['valor']);
        unset($dados['idarea']);
        unset($dados['descricaoarea']);
        unset($dados['descricaopropriedade']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function excluir($id){
        $sql = "DELETE FROM produto WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id'=>$id));
    }
}