<?php

class UsuarioModel extends Conexao{
    
    function __construct() {
        parent::__construct();
    }
    
    public function inserir(array $dados) {
        $sql   = "INSERT INTO usuario(nome, cpf, senha) "
                . " VALUES(:nome, :cpf, :senha)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function buscarTodos(){
        //verifica se é usuario comum ou se é o administrador que esta logado no sistema
        //Se for administrador terá acesso a todos os usuários, se não for administrador terá acesso apenas ao seu usuário
        $cpf = $_SESSION['cpf'];
        $verificausuariologado = "select administrador from usuario where cpf = '$cpf'";
        $sqlverificausuariologado = $this->bd->prepare($verificausuariologado);
        $sqlverificausuariologado->execute();
        
        foreach ($sqlverificausuariologado as $rs){
            $administrador = $rs["administrador"];
        }
        
        if($administrador == 'S'){
            //O usuário logado é administrador e terá acesso a todos os dados
            $sql   = "SELECT id, cpf, nome, senha, autorizado, administrador, tipousuario, to_char(datacadastro, 'dd/mm/YYYY') as datacadastro FROM usuario order by cpf asc;";
            $query = $this->bd->query($sql);
            return $query->fetchAll();
        }else if($administrador == 'N'){
            //Este usuário não é administrador então terá acesso apenas aos seus dados
            $sql   = "SELECT id, cpf, nome, senha, autorizado, administrador, tipousuario, to_char(datacadastro, 'dd/mm/YYYY') as datacadastro FROM usuario where cpf = '$cpf'";
            $query = $this->bd->query($sql);
            return $query->fetchAll();
        }
    }
    
    public function buscar($id) {
        $sql = "SELECT id, cpf, nome, senha FROM usuario "
                . " WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id'=>$id));
        
        return $query->fetch();
    }
    
    public function atualizar(array $dados) {
        $sql = "UPDATE usuario SET cpf = :cpf, nome = :nome,  senha = :senha WHERE id = :id";
        
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function excluir($id){
        $sql = "DELETE FROM usuario WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id'=>$id));
    }
    
    public function autorizar($id){
        //Verificar se o usuário esta autorizado ou não autorizado
        $consultastatususuario = "select autorizado from usuario where id = $id";
        $sqlconsultastatususuario = $this->bd->prepare($consultastatususuario);
        $sqlconsultastatususuario->execute();
        
        foreach ($sqlconsultastatususuario as $rs){
            $autorizado = $rs["autorizado"];
        }
        
        if($autorizado == 'S'){
            $sql = "UPDATE usuario set autorizado = 'N' WHERE id = :id";
            $query = $this->bd->prepare($sql);
            return $query->execute(array('id'=>$id));
        }else if($autorizado == 'N'){
            $sql = "UPDATE usuario set autorizado = 'S' WHERE id = :id";
            $query = $this->bd->prepare($sql);
            return $query->execute(array('id'=>$id));
        }
    }
}