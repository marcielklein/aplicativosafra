<?php

class AreaModel extends Conexao{
    
    function __construct() {
        parent::__construct();
    }
    
    public function inserir(array $dados) {
        $idpropriedade = $_POST['idpropriedade'];
        $codigoarea = $_POST['codigoarea'];
        
        //Consulta se o usuário já não possui o código da área cadastrado
        $consultaexistecodigoarea = pg_query("SELECT count(*) as existe
                                                FROM anosafra ano
                                               INNER JOIN area ar
                                                  ON ar.idano = ano.id
                                               INNER JOIN usuario u
                                                  ON u.id = ano.idusuario
                                               WHERE u.cpf = "  . "'" . $_SESSION['cpf'] . "'
                                                 AND ar.codigoarea = $codigoarea");
        $resconsultacodigoarea = pg_fetch_array($consultaexistecodigoarea);
        $codigoareaexiste = $resconsultacodigoarea['existe'];
        
        if($codigoareaexiste == "0"){
            //Consulta o ano que o usuário da seção possui na tabela anosafra
            $consultanousuario = pg_query("SELECT a.id as idano
                                             FROM propriedade p
                                            INNER JOIN anosafra a
                                               ON a.idpropriedade = p.id
                                            INNER JOIN usuario u
                                               ON u.id = a.idusuario
                                            WHERE u.cpf = " . "'" . $_SESSION['cpf'] . "'   
                                              AND p.id = $idpropriedade");
            $resconsultaanousuario = pg_fetch_array($consultanousuario);
            $idano = $resconsultaanousuario['idano'];

            $sql   = "INSERT INTO area(codigoarea, descricao, idpropriedade, idano, areatotal, areaplantio) "
                    . " VALUES(:codigoarea, :descricao, :idpropriedade, $idano, :areatotal, :areaplantio)";
            
            unset($dados['id']);
            unset($dados['propriedade']);
            unset($dados['idano']);
            $query = $this->bd->prepare($sql);
            return $query->execute($dados);
        }
    }
    
    public function buscarTodos(){
        $sql   = "SELECT a.id as id, 
                         a.codigoarea as codigoarea, 
                         a.descricao as descricao,
                         (a.codigoarea || ' - ' || a.descricao) as descricaoarea,
                         p.descricao as descricaopropriedade,
                         a.descricao as descricaonomearea
                    FROM area a
                   INNER JOIN propriedade p
                      ON a.idpropriedade = p.id
                   INNER JOIN anosafra ano
                      ON ano.idpropriedade = p.id
                   INNER JOIN usuario usu
                      ON ano.idusuario = usu.id
                   WHERE usu.cpf = " . "'" . $_SESSION['cpf'] . "' 
                   ORDER BY descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscar($id) {
        $sql = "SELECT id, codigoarea, descricao, areatotal, areaplantio FROM area WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id'=>$id));
        
        return $query->fetch();
    }
    
    public function areapropriedade($id) {
        $sql = "SELECT a.id,
                       pro.codigopropriedade,
                       pro.descricao as descricaopropriedade,
                       a.codigoarea,
                       a.descricao as descricaoarea,
                       a.idpropriedade,
                       a.areatotal as areatotal,
                       a.areaplantio as areaplantio
                  FROM area a
                 INNER JOIN propriedade pro
                    ON a.idpropriedade = pro.id
                 WHERE a.idpropriedade = :id
                 ORDER BY descricaopropriedade asc;";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id'=>$id));
        return $query->fetchAll();
    }
    
    public function atualizar(array $dados) {
        $codigoarea = $_POST['codigoarea'];
        $idarea = $_POST['id'];
        
        //Consulta o Código propriedade
        $consultaareaverpropriedade = pg_query("SELECT count(*) as existe
                                                  FROM propriedade pro 
                                                 INNER JOIN anosafra a 
                                                    ON a.idpropriedade = pro.id
                                                 INNER JOIN area ar
                                                    ON ar.idpropriedade = pro.id
                                                 INNER JOIN usuario u
                                                    ON u.id = a.idusuario
                                                 WHERE u.cpf = " . "'" . $_SESSION['cpf'] . "'
                                                   AND ar.codigoarea = $codigoarea
                                                   AND ar.id <> $idarea");
        
        $resconsultaareaverpropriedade = pg_fetch_array($consultaareaverpropriedade);
        $codigoexiste = $resconsultaareaverpropriedade['existe'];
        
        if($codigoexiste == "0"){
            $sql = "UPDATE area SET codigoarea = :codigoarea,  descricao = :descricao, areatotal = :areatotal, areaplantio = :areaplantio, idpropriedade = :idpropriedade WHERE id = :id";
            $query = $this->bd->prepare($sql);
            unset($dados['propriedade']);
            return $query->execute($dados);
        }
    }
    
    public function excluir($id){
        $sql = "DELETE FROM area WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id'=>$id));
    }
}