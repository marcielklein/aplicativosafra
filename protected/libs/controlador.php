<?php

require_once './protected/conexao.php';

Class Controlador{
   
    function __construct() {
        
        if(isset($_GET['controle'])){
            $ctrlNome = $_GET['controle'];
            
            $arquivo  = './protected/controller/'.$ctrlNome.'.php';
            
            if(file_exists($arquivo)){
                
                require $arquivo;
                
                $controle = new $ctrlNome();                
                    
                if($_GET['acao']=="novo" || $_GET['acao']=="listar"){
                    $controle->{$_GET['acao']}();
                }else if($_GET['acao']=="inserir" || $_GET['acao']=="atualizar"){
                    $controle->{$_GET['acao']}($_POST);
                }else if($_GET['acao']=="buscar" || $_GET['acao']=="excluir"){
                    $controle->{$_GET['acao']}($_GET['id']);
                }else if($_GET['acao']=="autorizar" || $_GET['acao']=="areapropriedade" || $_GET['acao']=="adicionarareapropriedade" || $_GET['acao']=="areaproduto"){
                    $controle->{$_GET['acao']}($_GET['id']);
                }
            }
        }else{?>
            <?php
                //Verificar se esta conectado com a internet
                if ($sock = @fsockopen('www.google.com.br', 80, $num, $error, 5)){
                    $conexaoexterna = true; 
                }else{
                    $conexaoexterna = false;
                }
            ?>
            <?php
                if($conexaoexterna == true){ ?>
                    <html>
                        <!--Begin of Weather Forecast-->
                        <div style="position:relative;background-color:#FFFFFF">
                        <div id="wf_div"></div>
                        <script type="text/javascript" src="https://tools.tititudorancea.com/weather_forecast.js?place=passo_fundo_brazil&amp;s=1&amp;days=7&amp;utf8=no&amp;columns=7&amp;lang=pt"></script>
                        <div style="font:10px Arial, sans-serif;color:#000000" align="right"><a href="https://www.tititudorancea.com/z/tempo_pt.htm">Previsão do tempo</a> 
                        fornecido por <a href="https://www.tititudorancea.com/">tititudorancea.com</a></div>
                        </div>
                        <!--End of Weather Forecast-->

                        <table>
                            <td height="375" width="215">
                                <div id="divCarregando" class="progresso">
                                    <p><img src="../../aplicativosafra/includes/imagens/loader.gif"/></p>
                                </div>
                                <div id="mime">
                                    <iframe height="375" width="215" src="https://ssltools.forexprostools.com/currency-converter/index.php?from=12&to=1590&force_lang=12">
                                    </iframe>
                                </div>
                                <br />
                            </td>
                            <td style="width: 20px;"></td>
                            <br/>
                            <td style="width: 80%; height: 380px; margin-top: 10px">
                                <iframe src="https://br.investingwidgets.com/live-indices?theme=darkTheme&pairs=166,27,172,177,170" width="100%" height="100%" frameborder="0" allowtransparency="true" marginwidth="0" marginheight="0">
                                </iframe>
                            </td>
                        </table>
                    </html>

                    <script>
                        var div = document.getElementById('mime');
                        div.style.display = 'none';
                        
                        setTimeout(function() {
                            div.style.display = 'block';
                        }, 18000);
                        
                        /*Mensagem de Aguarde*/
                        $(document).ready(function () {
                            $("#divCarregando").show();
                            $(window).load(function () {
                                // Quando a página estiver totalmente carregada, remove o id
                                $('#divCarregando').fadeOut('slow');
                            });
                        });
                    </script>
                <?php }else{
                    echo "<div style='color: red; text-align: center;'><b>Sem conexão externa... <br/> Não é possível carregar alguns conteúdos  :( </b></div>";
                }
            ?>
        <?php }
    }
}

