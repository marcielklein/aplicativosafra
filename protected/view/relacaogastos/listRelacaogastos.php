<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Gastos Propriedade</div>
            <div class="panel-body">
                <table class="table table-condensed" style="border-collapse:collapse;" id="example2">
                    <thead>
                        <tr>
                            <th>Código Propriedade</th>
                            <th>Nome da Propriedade</th>
                        </tr>
                    </thead>
                    <?php
                    //Buscar registros de propriedade, código da propriedade e nome da propriedade
                    $consultapropriedade = "SELECT pro.id as idpropriedade, 
                                   pro.codigopropriedade, 
                                   pro.descricao as nomepropriedade 
                              FROM propriedade pro
                             INNER JOIN anosafra ano
                                ON ano.idpropriedade = pro.id
                             INNER JOIN usuario usu
                                ON ano.idusuario = usu.id
                             WHERE usu.cpf = " . "'" . $_SESSION['cpf'] . "'
                               AND ano.ativo = 'A'
                             ORDER BY pro.codigopropriedade asc;";
                    $resultpropriedade = pg_query($consultapropriedade);
                    $contpropriedade = 0;
                    while ($consultapropriedade = pg_fetch_assoc($resultpropriedade)) {
                        //Contador
                        $contpropriedade = $contpropriedade + 1;
                        $democontlinhapropriedade = '#demo' . $contpropriedade;
                        $demoidlinhapropriedade = 'demo' . $contpropriedade;
                        ?>
                        <tbody>
                            <!-- LINHA 1 -->
                            <tr data-toggle="collapse" data-target="<?php echo $democontlinhapropriedade ?>" class="accordion-toggle" >
                                <td style="background-color: #E0E0E0"><?php echo $consultapropriedade['codigopropriedade']; ?></td>
                                <td style="background-color: #E0E0E0"><?php echo $consultapropriedade['nomepropriedade']; ?></td>
                                <?php $idparam_propriedade = $consultapropriedade['idpropriedade']; ?>
                            </tr>
                            <tr>
                                <td colspan="6" class="hiddenRow" style="padding-left: 0px;">
                                    <div class="accordian-body collapse" id="<?php echo $demoidlinhapropriedade ?>"> 
                                        <table class="table table-condensed" style="border-collapse:collapse;margin:15px">
                                            <?php
                                            $consultaarea = "select id as idarea, codigoarea, descricao as nomearea, areaplantio as areaplantio from area where idpropriedade = $idparam_propriedade";
                                            $resultarea = pg_query($consultaarea);
                                            $quantidadelinhaarea = pg_num_rows($resultarea);
                                            $contarea = 100;
                                            if ($quantidadelinhaarea == 0) {
                                                ?>
                                                <thead>
                                                    <tr>
                                                        <th style="color: red">Não existe Área para esta propriedade</th>
                                                    </tr>
                                                </thead>
                                            <?php
                                            } else {
                                                while ($consultaarea = pg_fetch_assoc($resultarea)) {
                                                    //Contador
                                                    $codigoareacont = $consultaarea['codigoarea'];
                                                    $contarea = $codigoareacont + 1;
                                                    $democontlinhaarea = '#demo' . $contarea;
                                                    $demoidlinhaarea = 'demo' . $contarea;
                                                    ?>
                                                    <thead>
                                                        <tr>
                                                            <th>Código da Área</th>
                                                            <th>Nome da Área</th>
                                                            <th>Área de Plantio (Hectares)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-toggle="collapse" data-target="<?php echo $democontlinhaarea ?>" class="accordion-toggle">
                                                            <td style="background-color: #FFFFE0"><?php echo $consultaarea['codigoarea']; ?></td>
                                                            <td style="background-color: #FFFFE0"><?php echo $consultaarea['nomearea']; ?></td>
                                                            <td style="background-color: #FFFFE0"><?php echo $consultaarea['areaplantio']; ?></td>
                                                        </tr>
                                                        <?php
                                                        //Parâmetro Área
                                                        $idparam_area = $consultaarea['idarea'];
                                                        ?>
                                                        <tr >
                                                            <td colspan="6" class="hiddenRow" style="padding-left: -10px;padding-left: 0px;padding-right: 20px;"><div class="accordian-body collapse" id="<?php echo $demoidlinhaarea ?>"> 

                                                                    <table class="table table-condensed" style="border-collapse:collapse;margin:15px">
                                                                        <?php
                                                                        $consultaproduto = "select id as idproduto, 
                                                                                        descricao as nomeproduto, 
                                                                                        quantidade, 
                                                                                        valor 
                                                                                   from produto 
                                                                                  where idarea = $idparam_area;";

                                                                        $resultproduto = pg_query($consultaproduto);
                                                                        $quantidadelinhaproduto = pg_num_rows($resultproduto);
                                                                        $contproduto = 0;
                                                                        if ($quantidadelinhaproduto == 0) {
                                                                            ?>
                                                                            <thead>
                                                                                <tr>
                                                                            <div style="color: red;">
                                                                                <b>Não existe Produto cadastrado para esta Área</b>
                                                                            </div>
                                                                            </tr>
                                                                            </thead>
                                                                                <?php } else { ?>
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Nome do Produto</th>
                                                                                    <th>Quantidade</th>
                                                                                    <th>Valor</th>
                                                                                    <th>Valor Total</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                while ($consultaproduto = pg_fetch_assoc($resultproduto)) {
                                                                                    //Contador
                                                                                    $contproduto = $contproduto + 1;
                                                                                    //aqui
                                                                                    ?>
                                                                                    <tr data-toggle="collapse" class="accordion-toggle">
                                                                                        <td><?php echo $consultaproduto['nomeproduto']; ?></td>
                                                                                        <td><?php echo $consultaproduto['quantidade']; ?></td>
                                                                                        <td class="text-success"><?php echo 'R$ ' . number_format($consultaproduto['valor'], 2, ',', '.'); ?></td>
                                                                                        <td class="text-success" style="color: blue">
                                                                                            <b>
                                                                                            <?php
                                                                                            $valortotal = $consultaproduto['valor'] * $consultaproduto['quantidade'];
                                                                                            echo 'R$ ' . number_format($valortotal, 2, ',', '.');
                                                                                            ?>
                                                                                            </b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                        </tbody>
                                                                    </table> 
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }
                                                }
                                                ?>    
                                        </table>
                                    </div> 
                                </td>
                            </tr>
                        </tbody>
                        <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>