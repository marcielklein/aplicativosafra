<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Propriedade</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formPropriedade" id="formPropriedade" method="POST" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($propriedade)) echo $propriedade['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="codigopropriedade">Código Propriedade</label>
                        <input type="text" class="form-control" id="codigopropriedade" name="codigopropriedade" placeholder="Digite o Código" onkeypress="return Onlynumbers(event)" 
                               value="<?php if (isset($propriedade)) echo $propriedade['codigopropriedade']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a descrição da propriedade" 
                               value="<?php if (isset($propriedade)) echo $propriedade['descricao']; ?>">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formPropriedade").validate({
        rules: {
            codigopropriedade: {
                required: true,
                maxlength: 6
            },
            descricao: {
                required: true,
                minlength: 2,
                maxlength: 100
            }
        },
        messages: {
            codigopropriedade: {
                required: "Por favor, informe o Código da Propriedade",
                maxlength: "O Código da Propriedade deve ter no máximo 6 caracteres"
            },
            descricao: {
                required: "Por favor, informe a Descrição da Propriedade",
                minlength: "A Descrição deve ter no mínimo 2 caracteres",
                maxlength: "A Descrição deve ter no máximo 100 caracteres"
            }
        }
    });
</script>