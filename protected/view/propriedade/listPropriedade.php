<?php if (isset($_GET['erro']) == 'area') { ?>
    <div class="alert alert-danger">
        Não foi possível inserir a área, pois o código da área já existe para este usuário.
    </div>
<?php } else if (isset($_GET['erroarea']) == '1') { ?>
    <div class="alert alert-danger">
        Não foi possível excluir a área, pois a área possui registros filhos.
    </div>
<?php }
?>

<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Propriedades</div>
            <div class="panel-body">
                <a href="index.php?controle=propriedadeController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>

                <table class="table table-striped table-hover" id="example2">
                    <thead>
                    <th>#</th>
                    <th>Código Propriedade</th>
                    <th>Descrição</th>
                    <th>Ano</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th style="width: 100px;">Adicionar Área</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['id'];
                            echo '<td>' . $item['codigopropriedade'];
                            echo '<td>' . $item['descricao'];
                            echo '<td>' . $item['ano'];
                            $id = $item['id'];
                            echo "<td> <a href='index.php?controle=propriedadeController&acao=buscar&id=$id'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a href='index.php?controle=propriedadeController&acao=excluir&id=$id'>"
                            . " <span class='glyphicon glyphicon-trash'> </span>"
                            . "</a> </td>";
                            echo "<td style='padding-left: 20px;'> <a href='index.php?controle=areaController&acao=areapropriedade&id=$id'>"
                            . " <span class='glyphicon' title='Adicionar Área'> Adicionar + </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>    
        </div>
    </div>
</div>