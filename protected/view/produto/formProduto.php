<?php
    if(isset($_GET['adicionarprodutoarea'])){
        $idarea = $_GET['adicionarprodutoarea'];
        $consultaareaproduto = pg_query("select a.descricao as descricaoarea,
                                                pro.descricao as descricaopropriedade
                                           from area a
                                          inner join propriedade pro
                                             on a.idpropriedade = pro.id
                                          where a.id = $idarea");
        $resconsultaareaproduto   = pg_fetch_array ($consultaareaproduto);
        $descricaoarea = $resconsultaareaproduto['descricaoarea'];
        $descricaopropriedade = $resconsultaareaproduto['descricaopropriedade'];
    }else if(isset($_GET['id']) != ''){
        $idarea = $_GET['id'];
        $sqlconsultaarea = pg_query("select a.descricao as descricaoarea,
                                            prop.descricao as descricaopropriedade
                                       from produto pro 
                                 inner join area a 
                                         on pro.idarea = a.id
                                 inner join propriedade prop
                                         on a.idpropriedade = prop.id
                                      where pro.id = $idarea");
        $resconsultaarea   = pg_fetch_array ($sqlconsultaarea);
        $descricaoarea = $resconsultaarea['descricaoarea'];
        $descricaopropriedade = $resconsultaarea['descricaopropriedade'];
    }
?>
<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formProduto" id="formProduto" method="POST" role="form">
                <input type="hidden" name="idproduto" value="<?php if (isset($produto)) echo $produto['id']; ?>"/>
                <input type="hidden" name="idarea" value="<?php echo $idarea?>"/>
                <div class="row">
                    <div class="col-md-8">
                        <label for="propriedade">Propriedade</label>
                        <input type="text" class="form-control" id="descricaopropriedade" name="descricaopropriedade" readonly="true" 
                               value="<?php echo $descricaopropriedade; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="area">Área</label>
                        <input type="text" class="form-control" id="descricaoarea" name="descricaoarea" readonly="true" 
                               value="<?php echo $descricaoarea; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a descrição do produto" 
                               value="<?php if (isset($produto)) echo $produto['descricao']; ?>">
                    </div>
                </div>
                    <div class="row">
                    <div class="col-md-3">
                        <label for="valor">Valor</label>
                        <input type="text" class="form-control" maxlength="20" id="valor" name="valor" placeholder="Digite o Valor" 
                               value="<?php if (isset($produto)) echo $produto['valor']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="quantidade">Quantidade</label>
                        <input type="text" class="form-control" id="quantidade" required="" name="quantidade" placeholder="Digite a Quantidade" 
                               value="<?php if (isset($produto)) echo $produto['quantidade']; ?>" onkeypress="return Onlynumbers(event)">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formProduto").validate({
        rules: {
            descricao: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            valor: {
                required: true
            },
            quantidade: {
                required: true
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a Descrição do Produto",
                minlength: "A Descrição deve ter no mínimo 2 caracteres",
                maxlength: "A Descrição deve ter no máximo 100 caracteres"
            },
            valor: {
                required: "Por favor, informe o Valor do Produto"
            },
            quantidade: {
                required: "Por favor, informe a Quantidade do Produto"
            }
        }
    });
</script>