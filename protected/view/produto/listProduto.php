<?php
if (isset($_GET['id'])) {
    $idarea = $_GET['id'];
    $sqlconsultaarea = pg_query("select a.id as idarea, 
                                            a.descricao as descricaoarea, 
                                            pro.descricao as descricaopropriedade 
                                       from area a 
                                 inner join propriedade pro 
                                         on a.idpropriedade = pro.id 
                                      where a.id = $idarea");
    $resconsultaarea = pg_fetch_array($sqlconsultaarea);
    $descricaoarea = $resconsultaarea['descricaoarea'];
    $descricaopropriedade = $resconsultaarea['descricaopropriedade'];
}
if (isset($_POST['idarea'])) {
    $idarea = $_POST['idarea'];
    $sqlconsultaarea = pg_query("select a.id as idarea, 
                                            a.descricao as descricaoarea,
                                            pro.descricao as descricaopropriedade
                                       from area a
                                      inner join propriedade pro
                                         on a.idpropriedade = pro.id
                                      where a.id = $idarea");
    $resconsultaarea = pg_fetch_array($sqlconsultaarea);
    $descricaoarea = $resconsultaarea['descricaoarea'];
    $descricaopropriedade = $resconsultaarea['descricaopropriedade'];
}
if (isset($_GET['idarea'])) {
    $idarea = $_GET['idarea'];
    $sqlconsultaarea = pg_query("select a.id as idarea, 
                                            a.descricao as descricaoarea,
                                            pro.descricao as descricaopropriedade
                                       from area a
                                      inner join propriedade pro
                                         on a.idpropriedade = pro.id
                                      where a.id = $idarea");
    $resconsultaarea = pg_fetch_array($sqlconsultaarea);
    $descricaoarea = $resconsultaarea['descricaoarea'];
    $descricaopropriedade = $resconsultaarea['descricaopropriedade'];
}
if (isset($_POST['idarea'])) {
    $idarea = $_POST['idarea'];
    $consultaareaproduto = pg_query("select a.descricao as descricaoarea,
                                                prop.descricao as descricaopropriedade
                                           from produto pro
                                          inner join area a
                                             on pro.idarea = a.id
                                           left join propriedade prop
                                             on a.idpropriedade = prop.id
                                          where pro.id = $idarea");
    $resconsultaareaproduto = pg_fetch_array($consultaareaproduto);
    $descricaoarea = $resconsultaareaproduto['descricaoarea'];
    $descricaopropriedade = $resconsultaareaproduto['descricaopropriedade'];
}


if((isset($_POST['idarea']) != '') && (isset($_POST['descricaopropriedade']) != '') && (isset($_POST['descricaoarea']) != '')){
    $consulta_ultimo_produto_add = pg_query("select max(id) as id from produto");
    $resconsulta_ultimo_produto_add = pg_fetch_array($consulta_ultimo_produto_add);
    $idprodutoultimo = $resconsulta_ultimo_produto_add['id'];
    
    $consultainfoaproduto = pg_query("select a.descricao as descricaoarea,
                                            prop.descricao as descricaopropriedade
                                       from produto pro
                                      inner join area a
                                         on pro.idarea = a.id
                                       left join propriedade prop
                                         on a.idpropriedade = prop.id
                                      where pro.id = $idprodutoultimo");
    $resconsultaprodutoultimo = pg_fetch_array($consultainfoaproduto);
    $descricaoarea = $resconsultaprodutoultimo['descricaoarea'];
    $descricaopropriedade = $resconsultaprodutoultimo['descricaopropriedade'];
}

?>
<div class="row">
    <div class="col-md-5">
        <label for="propriedade" style="font-size: 18px;">Propriedade:</label> <label style="font-size: 15px; font-weight:normal;"><?php echo $descricaopropriedade; ?></label><br/>
        <label for="propriedade" style="font-size: 18px;">Área:</label> <label style="font-size: 15px; font-weight:normal;"><?php echo $descricaoarea; ?></label>
    </div>
</div>
<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Produtos</div>
            <div class="panel-body">
                <?php
                echo " <a href='index.php?controle=produtoController&acao=novo&adicionarprodutoarea=$idarea'>"
                . "<span class='glyphicon glyphicon-plus'> Adicionar</span>"
                . "</a>";
                ?>

                <table class="table table-striped table-hover" id="example2">
                    <thead>
                    <th>#</th>
                    <th>Descrição</th>
                    <th>Valor</th>
                    <th>Quantidade</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaareaproduto as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['id'];
                            echo '<td>' . $item['descricaoproduto'];
                            echo '<td>' . $item['valorproduto'];
                            echo '<td style="padding-left: 20px;">' . $item['quantidadeproduto'];
                            $id = $item['id'];
                            $idarea = $item['idarea'];
                            echo "<td> <a href='index.php?controle=produtoController&acao=buscar&id=$id'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a href='index.php?controle=produtoController&acao=excluir&id=$id&idarea=$idarea'>"
                            . " <span class='glyphicon glyphicon-trash'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>