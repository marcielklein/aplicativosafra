<?php
require_once("../../../config/configdbrelatorio.php");
require_once("../../fpdf/fpdf.php");
 
$in_cpf_session = $_POST['cpfsessao'];
$in_idpropriedade = $_POST['idpropropriedade'];
$in_ano = $_POST['ano'];


if($in_idpropriedade != "" && $in_ano == ""){
  $filtropesquisa = 'where pro.id = ' . "$in_idpropriedade" . ' and usu.cpf = ' . "'$in_cpf_session'";
}else if($in_idpropriedade == "" && $in_ano != ""){
  $filtropesquisa = 'where ano.ano = ' . "$in_ano" . ' and usu.cpf = ' . "'$in_cpf_session'";
}else if($in_idpropriedade != "" && $in_ano != ""){
  $filtropesquisa = 'where pro.id = ' . "$in_idpropriedade" . ' and ano.ano = ' . "$in_ano" . ' and usu.cpf = ' . "'$in_cpf_session'";
}
else{
  $filtropesquisa = ' where usu.cpf = ' . "'$in_cpf_session'";
}

//SQL REFERENTE AO USUÁRIO
$query = "select nome as proprietario,
                 tipousuario,
                 to_char(datacadastro, 'dd/MM/yyyy') as datacadastro
            from usuario
           where cpf = " . "'$in_cpf_session'";
$result = pg_query($query);
    
 while ($consulta = pg_fetch_assoc($result)) {
        $proprietario = $consulta["proprietario"];
        $tipousuario = $consulta["tipousuario"];
        $datacadastro = $consulta["datacadastro"];
 }
 
 if($tipousuario == 'P'){
     $tipousuario = "Particular";
 }else if($tipousuario == 'C'){
     $tipousuario = "Comercial";
 }else if($tipousuario == 'A'){
     $tipousuario = "Administrador";
 }

//P = Paisagem
//L = Retrato
$pdf = new FPDF("P", "pt", "A4");

$pdf->AddPage('P');
$pdf->Ln(3);

$pdf->Line(585, 10, 10, 10);
$pdf->Line(10, 825, 10, 10);
$pdf->Line(585, 825, 585, 10);
$pdf->Line(585, 825, 10, 825);

$pdf->Line(135, 20, 15, 20);
$pdf->Line(15, 97, 15, 20);
$pdf->Image('../../../includes/imagens/safra.jpg', +20,30,110);
$pdf->Line(135, 97, 15, 97);
$pdf->Line(135, 97, 135, 20);

//Margens info proprietário
$pdf->Line(570, 20, 145, 20);
$pdf->Line(145, 97, 145, 20);
$pdf->Line(570, 97, 145, 97);
$pdf->Line(570, 97, 570, 20);

$pdf->Line(30, 10, 10, 10);
$pdf->Line(10, 30, 10, 10);

$pdf->SetFont('arial', 'B', 14);
$pdf->Cell(550, 7, utf8_decode('Relatório por Propriedade'), 0, 0, 'C');
$pdf->SetFont('arial', 'B', 11);
$pdf->Cell(-70, 7, utf8_decode(''), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Ln();$pdf->Ln();
$pdf->SetFont('arial', 'B');
$pdf->Cell(185, 7, utf8_decode('Proprietário: '), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Cell(122, 7, utf8_decode($proprietario), 0, 1, 'L');

$pdf->Cell(300, 7, utf8_decode(''), 0, 0, 'R');
$pdf->Ln();$pdf->Ln();
$pdf->SetFont('arial', 'B');
$pdf->Cell(197, 7, utf8_decode('Data Cadastro: '), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Cell(122, 7, utf8_decode($datacadastro), 0, 1, 'L');
$pdf->Ln();$pdf->Ln();
$pdf->SetFont('arial', 'B');
$pdf->Cell(189, 7, utf8_decode('Tipo Usuário: '), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Cell(122, 7, utf8_decode($tipousuario), 0, 1, 'L');

$pdf->Ln();$pdf->Ln();

//DADOS DA TABELA
$pdf->SetFont('Arial', '', 8);
$query = "select pro.id as idpropriedade,
                 pro.codigopropriedade,
                 pro.descricao as descricaopropriedade,
                 ano.ano as anosafra
            from propriedade pro
      inner join anosafra ano 
              on pro.id = ano.idpropriedade
      inner join usuario usu
              on ano.idusuario = usu.id
                 $filtropesquisa";
$result = pg_query($query);

while ($consulta = pg_fetch_assoc($result)) {
	//CABÇALHO RELATÓRIO PROPRIEDADE
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->setFillColor(180, 180, 180);
        $pdf->Cell(100, 14, utf8_decode('Código Propriedade'), 1, 0, 'L', 1);
        $pdf->Cell(343, 14, utf8_decode('Nome Propriedade'), 1, 0, 'L', 1);
        $pdf->Cell(92, 14, utf8_decode('Ano Safra'), 1, 0, 'C', 1);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Ln();
        
        $pdf->SetFillColor(255, 255, 255);
	$pdf->Cell(100, 12, utf8_decode($consulta['codigopropriedade']), 1, 0, 'L', 1);
	$pdf->Cell(343, 12, utf8_decode($consulta['descricaopropriedade']), 1, 0, 'L', 1);
        $pdf->Cell(92, 12, utf8_decode($consulta['anosafra']), 1, 0, 'C', 1);
        $id_param_propriedade = $consulta['idpropriedade'];
        
        $pdf->Ln();
        //SQL ÁREA
        $queryarea = "select id as idarea,
                             codigoarea,
                             descricao as nomearea,
                             areatotal,
                             areaplantio
                        from area
                       where idpropriedade = $id_param_propriedade";
        $resultarea = pg_query($queryarea);
        
        
        //CABÇALHO RELATÓRIO ÁREA
       while ($consultaarea = pg_fetch_assoc($resultarea)) {
               $pdf->SetFont('Arial', 'B', 8);
               $pdf->setFillColor(180, 180, 180);
               $pdf->Cell(100, 14, utf8_decode('Código Área'), 1, 0, 'L', 1);
               $pdf->Cell(250, 14, utf8_decode('Nome Área'), 1, 0, 'L', 1);
               $pdf->Cell(93, 14, utf8_decode('Área Total (Hectare)'), 1, 0, 'C', 1);
               $pdf->Cell(92, 14, utf8_decode('Área Plantio (Hectare)'), 1, 0, 'C', 1);
               $pdf->Ln();
               $pdf->SetFont('Arial', '', 8);
        
               $pdf->SetFillColor(255, 255, 255);
               $pdf->SetFont('Arial', '', 8);
               $pdf->Cell(100, 12, utf8_decode($consultaarea['codigoarea']), 1, 0, 'L', 1);
               $pdf->Cell(250, 12, utf8_decode($consultaarea['nomearea']), 1, 0, 'L', 1);
               $pdf->Cell(93, 12, utf8_decode($consultaarea['areatotal']), 1, 0, 'C', 1);
               $pdf->Cell(92, 12, utf8_decode($consultaarea['areaplantio']), 1, 0, 'C', 1);
               $pdf->SetFont('Arial', '', 8);
               $id_param_area = $consultaarea['idarea'];
               
               //CABÇALHO RELATÓRIO PRODUTO
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->setFillColor(180, 180, 180);
                
                $queryproduto = "select descricao,
                                        valor,
                                        quantidade
                                   from produto
                                  where idarea = $id_param_area";
               $resultproduto = pg_query($queryproduto);
                
                $pdf->Cell(350, 14, utf8_decode('Produto'), 1, 0, 'L', 1);
                $pdf->Cell(93, 14, utf8_decode('Quantidade'), 1, 0, 'C', 1);
                $pdf->Cell(92, 14, utf8_decode('Valor'), 1, 0, 'C', 1);
                while ($consultaproduto = pg_fetch_assoc($resultproduto)) {
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Ln();
                    $pdf->SetFillColor(255, 255, 255);
                    if($consultaproduto['descricao'] == ""){
                        $pdf->Cell(350, 12, "Não há produtos para serem exibidos", 1, 0, 'L', 1);
                    }else{
                        $pdf->Cell(350, 12, utf8_decode($consultaproduto['descricao']), 1, 0, 'L', 1);
                        $pdf->Cell(93, 12, utf8_decode($consultaproduto['quantidade']), 1, 0, 'C', 1);
                        $pdf->Cell(92, 12, "R$ " . number_format($consultaproduto['valor'], 2, ',', '.'), 1, 0, 'C', 1);
                    }
                }
                
                $pdf->SetFont('Arial', '', 8);
                
               $pdf->Ln();
       }
}

$pdf->Output("rel_relatoriopropriedade.pdf", "D");

pg_close($conexao);