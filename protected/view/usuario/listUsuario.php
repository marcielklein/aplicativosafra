<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Usuários</div>
            <div class="panel-body">
            </div>
            <?php
            $cpf = $_SESSION['cpf'];
            $sqlusuariologado = pg_query("select administrador from usuario where cpf = '$cpf'");
            $resusuariologado = pg_fetch_array($sqlusuariologado);
            $usuariologado = $resusuariologado['administrador'];
            ?>
            <table class="table table-striped table-hover" id="example2">
                <thead>
                <th>#</th>
                <th>Nome</th>
                <th>CPF</th>
                <th>Tipo Usuário</th>
                <th>Data Cadastro</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <?php if ($usuariologado == 'S') { ?>
                    <th>Autorizar</th>
                <?php } ?>
                </thead>
                <tbody>
                    <?php
                    foreach ($listaDados as $item) {
                        echo '<tr>';
                        echo '<td>' . $item['id'];
                        echo '<td>' . $item['nome'];
                        echo '<td>' . $item['cpf'];
                        if ($item['tipousuario'] == 'A') {
                            echo '<td>' . 'Administrador';
                        } else if ($item['tipousuario'] == 'P') {
                            echo '<td>' . 'Particular';
                        } else if ($item['tipousuario'] == 'C') {
                            echo '<td>' . 'Comercial';
                        }
                        echo '<td>' . $item['datacadastro'];
                        $id = $item['id'];
                        $autorizado = $item['autorizado'];
                        $administrador = $item['administrador'];
                        echo "<td></td>";
                        echo "<td></td>";
                        /*
                         * Verificar com o professor
                         * echo "<td> <a href='index.php?controle=usuarioController&acao=buscar&id=$id'>"
                          . " <span class='glyphicon glyphicon-pencil' title='Alterar'> </span>"
                          . "</a> </td>";
                          echo "<td> <a href='index.php?controle=usuarioController&acao=excluir&id=$id'>"
                          . " <span class='glyphicon glyphicon-trash' title='Excluir'> </span>"
                          . "</a> </td>"; */
                        if ($usuariologado == 'S') {
                            if ($autorizado == 'S') {
                                if ($administrador != 'S') {
                                    echo "<td style='width: 80px; padding-left: 38px;'>"
                                    . "<a href='index.php?controle=usuarioController&acao=autorizar&id=$id'>"
                                    . "<img src='../../aplicativosafra/includes/imagens/naoautorizado.png' height='20' width='20' title='Bloquear'/></a>";
                                }
                            } else if ($autorizado == 'N') {
                                echo "<td style='width: 80px; padding-left: 38px;'>"
                                . "<a href='index.php?controle=usuarioController&acao=autorizar&id=$id'>"
                                . "<img src='../../aplicativosafra/includes/imagens/autorizado.png' height='20' width='20' title='Permitir'/></a>";
                            }
                        }
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>