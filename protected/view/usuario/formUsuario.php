<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Cadastro de Usuário</div>
            <div class="panel-body">
                <form action="<?php echo $acao; ?>" name="formUsuario" method="POST" role="form">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="id">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                   value="<?php if (isset($usuario)) echo $usuario['id']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="nome" required="" name="nome" placeholder="Digite o Nome" 
                                   value="<?php if (isset($usuario)) echo $usuario['nome']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="cpf">CPF</label>
                            <input type="text" class="form-control" name="cpf" required="" id="cpf" placeholder="Digite o CPF" 
                                   value="<?php if (isset($usuario)) echo $usuario['cpf']; ?>" onBlur="ValidarCPF(formUsuario.cpf);">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control" id="senha" required="" name="senha" placeholder="Digite a Senha" 
                                   value="<?php if (isset($usuario)) echo $usuario['senha']; ?>">
                        </div>
                    </div>
                    <br/>
                    <button type="submit" class="btn btn-success">Gravar</button>
                    <button type="reset" class="btn btn-primary">Limpar</button>
                </form>
            </div>
        </div>
    </div>
</div>
