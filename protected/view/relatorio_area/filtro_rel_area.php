<?php
    require_once("../../../config/configdbrelatorio.php");
    session_start();
    if(isset($_SESSION['cpf']) != ''){
        $sessao_cpf = $_SESSION['cpf'];
    }else{
        echo "<script>top.location.href='../../../acesso/login.php';</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Aplicativo Safra</title>
        <link href="../../../includes/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../includes/css/datatables.min.css">
        <link rel="stylesheet" href="../../../includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="../../../includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="../../../includes/js/validarcampos.js" type="text/javascript"></script>
        
        <script src="../../../includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="../../../includes/css/jquery-ui.css" type="text/css" />
       
        <style>
            #menutitle{
                color: white;
                padding-left: 25px;
            }
            .ui-datepicker .ui-datepicker-header {
                position: relative;
                padding: .2em 0;
                background-color: #3D5B99;
            }
            div.ui-datepicker{
             font-size:12px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #006400">
            <div class="container" style="padding-left: 0px">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#" style="height: 30px; width: 300px;"  id="menutitle">Filtro Relatório por Área</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="width: 120%;"></div>
            </div>
        </nav>
        <br/><br/><br/>
        <div class="container">
        <form name="filtrorelatorioarea" id="filtrorelatorioarea" action="rel_area.php" method="post">
            <input type="hidden" name="cpfsession" value="<?php echo $sessao_cpf;?>" />
            <div class="form-group">
                <label>Área</label>
                <select name="idarea" id="idarea" class="form-control">
                    <?php
                    $sqlArea = "select are.id as idarea,
                                       are.descricao as nomearea 
                                  from anosafra ano
                                 inner join propriedade pro
                                    on ano.idpropriedade = pro.id
                                 inner join area are
                                    on are.idpropriedade = pro.id
                                 inner join usuario usu
                                    on ano.idusuario = usu.id
                                 where usu.cpf = " . "'" .  $sessao_cpf . "'
                                 order by nomearea asc; ";
                    $sqlAreaResult = pg_query($sqlArea);
                    echo '<option value="">
                            Selecione...
                        </option>';
                    while ($sqlAreaResults = pg_fetch_assoc($sqlAreaResult)) {
                        ?>
                        <option value="<?php echo $sqlAreaResults["idarea"] ?>">
                            <?php echo $sqlAreaResults["nomearea"] ?>
                        </option>

                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="row">
                <div class="col-xs-3">
                    <label for="ano">Ano </label>
                    <input class="form-control" id="ano" name="ano" onkeypress="return Onlynumbers(event)"/>
                </div>
            </div>            
            <br/>
            <button type="submit" class="btn btn-success">Gerar Relatório</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
        </form>
    </div>
    </body>
</html>