<?php

require_once("../../../config/configdbrelatorio.php");
require_once("../../fpdf/fpdf.php");

$in_cpf_session = $_POST['cpf_session'];
$in_idcultura = $_POST['idcultura'];
$in_ano = $_POST['ano'];


if ($in_idcultura != "" && $in_ano == "") {
    $filtropesquisa = 'where cul.id = ' . "$in_idcultura" . ' and usu.cpf = ' . "'$in_cpf_session'";
} else if ($in_idcultura == "" && $in_ano != "") {
    $filtropesquisa = 'where cul.anosafra = ' . "$in_ano" . ' and usu.cpf = ' . "'$in_cpf_session'";
} else if ($in_idcultura != "" && $in_ano != "") {
    $filtropesquisa = 'where cul.id = ' . "$in_idcultura" . ' and cul.anosafra = ' . "$in_ano" . ' and usu.cpf = ' . "'$in_cpf_session'";
} else {
    $filtropesquisa = ' where usu.cpf = ' . "'$in_cpf_session'";
}

//SQL REFERENTE AO USUÁRIO
$query = "select nome as proprietario,
                 tipousuario,
                 to_char(datacadastro, 'dd/MM/yyyy') as datacadastro
            from usuario
           where cpf = " . "'$in_cpf_session'";
$result = pg_query($query);

while ($consulta = pg_fetch_assoc($result)) {
    $proprietario = $consulta["proprietario"];
    if($consulta["tipousuario"] == 'A'){
        $tipousuario = "Administrador";
    }else if($consulta["tipousuario"] == 'P'){
        $tipousuario = "Particular";
    }else if($consulta["tipousuario"] == 'C'){
        $tipousuario = "Comercial";
    }
    
    $datacadastro = $consulta["datacadastro"];
}

if ($tipousuario == 'P') {
    $tipousuario = "Particular";
} else if ($tipousuario == 'C') {
    $tipousuario = "Comercial";
}

//P = Paisagem
//L = Retrato
$pdf = new FPDF("P", "pt", "A4");

$pdf->AddPage('P');
$pdf->Ln(3);

$pdf->Line(585, 10, 10, 10);
$pdf->Line(10, 825, 10, 10);
$pdf->Line(585, 825, 585, 10);
$pdf->Line(585, 825, 10, 825);

$pdf->Line(135, 20, 15, 20);
$pdf->Line(15, 97, 15, 20);
$pdf->Image('../../../includes/imagens/safra.jpg', +20, 30, 110);
$pdf->Line(135, 97, 15, 97);
$pdf->Line(135, 97, 135, 20);

//Margens info proprietário
$pdf->Line(570, 20, 145, 20);
$pdf->Line(145, 97, 145, 20);
$pdf->Line(570, 97, 145, 97);
$pdf->Line(570, 97, 570, 20);

$pdf->Line(30, 10, 10, 10);
$pdf->Line(10, 30, 10, 10);

$pdf->SetFont('arial', 'B', 14);
$pdf->Cell(550, 7, utf8_decode('Relatório por Cultura'), 0, 0, 'C');
$pdf->SetFont('arial', 'B', 11);
$pdf->Cell(-70, 7, utf8_decode(''), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('arial', 'B');
$pdf->Cell(185, 7, utf8_decode('Proprietário: '), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Cell(122, 7, utf8_decode($proprietario), 0, 1, 'L');

$pdf->Cell(300, 7, utf8_decode(''), 0, 0, 'R');
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('arial', 'B');
$pdf->Cell(197, 7, utf8_decode('Data Cadastro: '), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Cell(122, 7, utf8_decode($datacadastro), 0, 1, 'L');
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('arial', 'B');
$pdf->Cell(189, 7, utf8_decode('Tipo Usuário: '), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Cell(122, 7, utf8_decode($tipousuario), 0, 1, 'L');

$pdf->Ln();
$pdf->Ln();

//DADOS DA TABELA
$pdf->SetFont('Arial', '', 8);
$query = "select cul.id as idcultura,
                 cul.descricao as descricaocultura,
                 cul.anosafra,
                 cul.idarea as idarea,
                 col.peso as pesocolheita
            from cultura cul 
            inner join usuario usu
              on cul.idusuario = usu.id
           inner join plantio plan
              on plan.idcultura = cul.id
           inner join colheita col
              on plan.id = col.idplantio
                 $filtropesquisa";
$result = pg_query($query);

while ($consulta = pg_fetch_assoc($result)) {
    //CABÇALHO RELATÓRIO CULTURA
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setFillColor(180, 180, 180);
    $pdf->Cell(335, 14, utf8_decode('Nome Cultura'), 1, 0, 'L', 1);
    $pdf->Cell(100, 14, utf8_decode('Ano Cultura'), 1, 0, 'C', 1);
    $pdf->Cell(100, 14, utf8_decode('Peso Colheita'), 1, 0, 'C', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->Ln();

    $pdf->SetFillColor(255, 255, 255);
    $pdf->Cell(335, 12, utf8_decode($consulta['descricaocultura']), 1, 0, 'L', 1);
    $pdf->Cell(100, 12, utf8_decode($consulta['anosafra']), 1, 0, 'C', 1);
    $pdf->Cell(100, 12, utf8_decode($consulta['pesocolheita'] . " Kg"), 1, 0, 'C', 1);
    $id_param_area = $consulta['idarea'];
    $pdf->Ln();

    //SQL AREA
    $queryarea = "select codigoarea,
                         descricao as nomearea,
                         id as idarea
                    from area
                   where id = $id_param_area";
    $resultarea = pg_query($queryarea);

    while ($consultaarea = pg_fetch_assoc($resultarea)) {
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->setFillColor(180, 180, 180);
        $pdf->Cell(100, 14, utf8_decode('Código Área'), 1, 0, 'L', 1);
        $pdf->Cell(435, 14, utf8_decode('Nome Área'), 1, 0, 'L', 1);
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 8);

        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(100, 12, utf8_decode($consultaarea['codigoarea']), 1, 0, 'L', 1);
        $pdf->Cell(435, 12, utf8_decode($consultaarea['nomearea']), 1, 0, 'L', 1);
        $pdf->SetFont('Arial', '', 8);
        $id_param_area = $consultaarea['idarea'];
        $pdf->Ln();

        //SQL PROPRIEDADE
        $querypropriedade = "select pro.id as idpropriedade,
                             pro.codigopropriedade,
                             pro.descricao as nomepropriedade,
                             are.id as idarea
                        from propriedade pro
                       inner join area are
                          on pro.id = are.idpropriedade
                       where are.id = $id_param_area";
        $resultpropriedade = pg_query($querypropriedade);
        //CABÇALHO RELATÓRIO PROPRIEDADE
        while ($consultapropriedade = pg_fetch_assoc($resultpropriedade)) {
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->setFillColor(180, 180, 180);
            $pdf->Cell(100, 14, utf8_decode('Código Propriedade'), 1, 0, 'L', 1);
            $pdf->Cell(435, 14, utf8_decode('Nome Propriedade'), 1, 0, 'L', 1);
            $pdf->Ln();
            $pdf->SetFont('Arial', '', 8);

            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(100, 12, utf8_decode($consultapropriedade['codigopropriedade']), 1, 0, 'L', 1);
            $pdf->Cell(435, 12, utf8_decode($consultapropriedade['nomepropriedade']), 1, 0, 'L', 1);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Ln();
        }
    }
}

$pdf->Output("rel_relatoriocultura.pdf", "D");

pg_close($conexao);
