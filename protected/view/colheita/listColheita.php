<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Colheita</div>
            <div class="panel-body">
                <a href="index.php?controle=colheitaController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                        <th style="font-size: 14px;">ID</th>
                        <th style="font-size: 14px;">Plantio</th>
                        <th style="font-size: 14px;">Peso</th>
                        <th style="font-size: 14px;">Ano</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td style="border-left-width: 10px; padding-right: 0px; padding-left: 21px; font-size: 14px;">' . $item['id'];
                            echo '<td style="font-size: 14px;">' . $item['descricaocultura'];
                            echo '<td style="font-size: 14px;">' . $item['peso'];
                            echo '<td style="font-size: 14px;">' . $item['ano'];
                            $id = $item['id'];
                            
                             echo "<td style='font-size: 14px;'> <a href='index.php?controle=colheitaController&acao=buscar&id=$id'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td style='font-size: 14px;'> <a href='index.php?controle=colheitaController&acao=excluir&id=$id'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>