<?php
    $ano_automatico = date('Y');
?>
<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Colheita</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formColheita" id="formColheita" method="POST" class="form" role="form">
                <input type="hidden" name="ano" value="<?php echo $ano_automatico?>" />
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($colheita)) echo $colheita['id']; ?>">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <label for="idplantio">Plantio</label>
                        <select class="form-control" name="idplantio" id="idplantio">
                            <option value="">Selecione o Plantio</option>>
                            <?php
                            foreach ($listaPlantios as $plantios) {
                                $selected = (isset($colheita) && $colheita['idplantio'] == $plantios['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $plantios['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $plantios['descricaocultura']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-2">
                        <label for="peso">Peso (Kg)</label>
                        <input type="text" class="form-control" id="peso" name="peso" placeholder="Digite o Peso" 
                               value="<?php if (isset($colheita)) echo $colheita['peso']; ?>" maxlength="10" required="" onkeypress="return Onlynumbers(event)">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="ano">Ano</label>
                        <input type="text" class="form-control" id="ano" name="ano" placeholder="Digite o Ano" 
                               value="<?php echo $ano_automatico ?>" disabled="" minlength="4" maxlength="4" required="" onkeypress="return Onlynumbers(event)">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formColheita").validate({
        rules: {
            idplantio: {
                required: true
            },
            peso: {
                required: true
            }
        },
        messages: {
            idplantio: {
                required: "Por favor, Selecione o Plantio"
            },
            peso: {
                required: "Por favor, Informe o Peso"
            }
        }
    });
</script>
