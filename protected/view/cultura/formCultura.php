<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Cultura</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formCultura" id="formCultura" method="POST" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($cultura)) echo $cultura['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a descrição da cultura" 
                               value="<?php if (isset($cultura)) echo $cultura['descricao']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="idarea">Área</label>
                        <select class="form-control" name="idarea" id="idarea">
                            <option value="">Selecione a Área</option>
                            <?php
                                foreach ($listaAreas as $areas) {
                                    $selected = (isset($cultura) && $cultura['idarea'] == $areas['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $areas['id']; ?>'
                                            <?php echo $selected; ?>> 
                                            <?php echo "Propriedade: " . $areas['descricaopropriedade'] . " - Área: " . $areas['descricaonomearea']?>
                                    </option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formCultura").validate({
        rules: {
            descricao: {
                required: true,
                minlength: 2,
                maxlength: 50
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a Descrição da Cultura",
                minlength: "A Descrição deve ter no mínimo 2 caracteres",
                maxlength: "A Descrição deve ter no máximo 50 caracteres"
            }
        }
    });
</script>