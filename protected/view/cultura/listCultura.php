<?php if (isset($_GET['erro']) == 'area') { ?>
    <div class="alert alert-danger">
        Não foi possível inserir a cultura, pois a cultura já encontra-se cadastrada para este usuário.
    </div>
<?php } else if (isset($_GET['errocultura']) == '1') { ?>
    <div class="alert alert-danger">
        Não foi possível excluir a cultura, pois a cultura possui registros filhos.
    </div>
<?php }
?>

<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Cultura</div>
            <div class="panel-body">
                <a href="index.php?controle=culturaController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <table class="table table-striped table-hover" id="example2">
                <thead>
                <th>#</th>
                <th>Descrição</th>
                <th>Área Propriedade</th>
                <th>Ano</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>        
                </thead>
                <tbody>
                    <?php
                    foreach ($listaDados as $item) {
                        echo '<tr>';
                        echo '<td>' . $item['id'];
                        echo '<td>' . $item['descricao'];
                        echo '<td>' . $item['descricaoarea'] . ' - ' . $item['descricaopropriedade'];
                        echo '<td>' . $item['anosafra'];
                        $id = $item['id'];
                        echo "<td> <a href='index.php?controle=culturaController&acao=buscar&id=$id'>"
                        . " <span class='glyphicon glyphicon-pencil'> </span>"
                        . "</a> </td>";
                        echo "<td> <a href='index.php?controle=culturaController&acao=excluir&id=$id'>"
                        . " <span class='glyphicon glyphicon-trash'> </span>"
                        . "</a> </td>";
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
