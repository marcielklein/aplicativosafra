<?php
    $ano_automatico = date('Y');
?>
<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Plantio</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formPlantio" id="formPlantio" method="POST" class="form" role="form">
                <input type="hidden" name="ano" value="<?php echo $ano_automatico?>" />
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($plantio)) echo $plantio['id']; ?>">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <label for="idcultura">Cultura</label>
                        <select class="form-control" name="idcultura" id="idcultura">
                            <option value="">Selecione a Cultura</option>
                            <?php
                                foreach ($listaCulturas as $culturas) {
                                    $selected = (isset($plantio) && $plantio['idcultura'] == $culturas['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $culturas['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $culturas['descricao']; ?>
                                    </option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <label for="idarea">Área</label>
                        <select class="form-control" name="idarea" id="idarea">
                            <option value="">Selecione a Área</option>>
                            <?php
                            foreach ($listaAreas as $areas) {
                                $selected = (isset($plantio) && $plantio['idarea'] == $areas['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $areas['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $areas['descricaoarea']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="ano">Ano</label>
                        <input type="text" class="form-control" id="ano" name="ano" placeholder="Digite o Ano" 
                               value="<?php echo $ano_automatico ?>" disabled="" minlength="4" maxlength="4" required="" onkeypress="return Onlynumbers(event)">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formPlantio").validate({
        rules: {
            idcultura: {
                required: true
            },
            idarea: {
                required: true
            }
        },
        messages: {
            idcultura: {
                required: "Por favor, Selecione a Cultura"
            },
            idarea: {
                required: "Por favor, Selecione a Área"
            }
        }
    });
</script>
