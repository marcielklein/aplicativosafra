<?php
    if(isset($_GET['adicionarareapropriedade']) != ''){
        $idpropriedade = $_GET['adicionarareapropriedade'];
        $sqlconsultapropriedade = pg_query("select descricao as descricaopropriedade from propriedade where id = $idpropriedade");
        $resconsultapropriedade   = pg_fetch_array ($sqlconsultapropriedade);
        $descricaopropriedade = $resconsultapropriedade['descricaopropriedade'];
    }else if(isset($_GET['id']) != ''){
        $id = $_GET['id'];
        $sqlconsultapropriedade = pg_query("select p.id as idpropriedade, p.descricao as descricaopropriedade from area a inner join propriedade p on a.idpropriedade = p.id where a.id = $id");
        $resconsultapropriedade   = pg_fetch_array ($sqlconsultapropriedade);
        $descricaopropriedade = $resconsultapropriedade['descricaopropriedade'];
        $idpropriedade = $resconsultapropriedade['idpropriedade'];
    }
?>

<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Área</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formArea" id="formArea" method="POST" role="form">
                <?php
                if($idpropriedade == ''){ ?>
                    <div class="row">
                        <div class="col-md-1">
                            <label for="id">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                   value="<?php if (isset($area)) echo $area['id']; ?>">
                        </div>
                    </div>
                   <?php }
                ?>
                <input type="hidden" name="id" value="<?php if (isset($area)) echo $area['id']; ?>"/>
                <input type="hidden" name="idpropriedade" value="<?php echo $idpropriedade?>"/>
                <div class="row">
                    <div class="col-md-8">
                        <label for="propriedade">Propriedade</label>
                        <input type="text" class="form-control" id="id" name="propriedade" readonly="true" 
                               value="<?php echo $descricaopropriedade; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="codigoarea">Código Área</label>
                        <input type="text" class="form-control" id="codigoarea" name="codigoarea" placeholder="Digite o Código" onkeypress="return Onlynumbers(event)" 
                               value="<?php if (isset($area)) echo $area['codigoarea']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" required="" placeholder="Digite a descrição da Área" 
                               value="<?php if (isset($area)) echo $area['descricao']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="areatotal">Área Total</label>
                        <input type="text" class="form-control" id="areatotal" name="areatotal" required="" placeholder="Digite a Área Total"
                               value="<?php if (isset($area)) echo $area['areatotal']; ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="areaplantio">Área Plantio</label>
                        <input type="text" class="form-control" id="areaplantio" name="areaplantio" required="" placeholder="Digite a Área Plantio" 
                               value="<?php if (isset($area)) echo $area['areaplantio']; ?>">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formArea").validate({
        rules: {
            codigoarea: {
                required: true,
                maxlength: 6
            },
            descricao: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            areatotal: {
                required: true,
                maxlength: 10
            },
            areaplantio: {
                required: true,
                maxlength: 10
            }
        },
        messages: {
            codigoarea: {
                required: "Por favor, informe o Código da Área",
                maxlength: "O Código da Área deve ter no máximo 6 caracteres"
            },
            descricao: {
                required: "Por favor, informe a Descrição da Área",
                minlength: "A Descrição deve ter no mínimo 2 caracteres",
                maxlength: "A Descrição deve ter no máximo 100 caracteres"
            },
            areatotal: {
                required: "Por favor, informe a Área Total",
                maxlength: "A Descrição deve ter no máximo 10 caracteres"
            },
            areaplantio: {
                required: "Por favor, informe a Área Total do Plantio",
                maxlength: "A Descrição deve ter no máximo 10 caracteres"
            }
        }
    });
</script>