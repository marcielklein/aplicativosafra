<?php
if (isset($_GET['id'])) {
    $idpropriedade = $_GET['id'];
    $sqlconsultapropriedade = pg_query("select descricao as descricaopropriedade from propriedade where id = $idpropriedade");
    $resconsultapropriedade = pg_fetch_array($sqlconsultapropriedade);
    $descricaopropriedade = $resconsultapropriedade['descricaopropriedade'];
    //Busca a descrição para propriedade ao excluir o registro
    if (isset($_GET['idpropriedade'])) {
        $idpropriedade = $_GET['idpropriedade'];
        $sqlconsultapropriedade = pg_query("select descricao as descricaopropriedade from propriedade where id = $idpropriedade");
        $resconsultapropriedade = pg_fetch_array($sqlconsultapropriedade);
        $descricaopropriedade = $resconsultapropriedade['descricaopropriedade'];
    }
} else if (isset($_POST['idpropriedade'])) {
    $idpropriedade = $_POST['idpropriedade'];
    $sqlconsultapropriedade = pg_query("select descricao as descricaopropriedade from propriedade where id = $idpropriedade");
    $resconsultapropriedade = pg_fetch_array($sqlconsultapropriedade);
    $descricaopropriedade = $resconsultapropriedade['descricaopropriedade'];
}
?>
<div class="row">
    <div class="col-md-5">
        <label for="propriedade" style="font-size: 18px;">Propriedade:</label> <label style="font-size: 15px; font-weight:normal;"><?php echo $descricaopropriedade; ?></label>
    </div>
</div>
<div id="fundo">
    <div class="col-md-12 col-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Áreas</div>
            <div class="panel-body">
                <?php
                echo " <a href='index.php?controle=areaController&acao=novo&adicionarareapropriedade=$idpropriedade'>"
                . "<span class='glyphicon glyphicon-plus'> Adicionar</span>"
                . "</a>";
                ?>
            </div>
            <br/>

            <table class="table table-striped table-hover" id="example2">
                <thead>
                <th>#</th>
                <th>Código Área</th>
                <th>Descrição</th>
                <th>Área Total</th>
                <th>Área Plantio</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th style="width: 150px;">Adicionar Produtos</th>
                </thead>
                <tbody>
                    <?php
                    foreach ($listaareapropriedade as $item) {
                        echo '<td>' . $item['id'];
                        echo '<td>' . $item['codigoarea'];
                        echo '<td>' . $item['descricaoarea'];
                        echo '<td style="padding-left: 20px;">' . $item['areatotal'];
                        echo '<td style="padding-left: 20px;">' . $item['areaplantio'];
                        $id = $item['id'];
                        $idpropriedade = $item['idpropriedade'];
                        echo "<td> <a href='index.php?controle=areaController&acao=buscar&id=$id'>"
                        . " <span class='glyphicon glyphicon-pencil'> </span>"
                        . "</a> </td>";
                        echo "<td> <a href='index.php?controle=areaController&acao=excluir&id=$id&idpropriedade=$idpropriedade'>"
                        . " <span class='glyphicon glyphicon-trash'> </span>"
                        . "</a> </td>";
                        echo "<td style='padding-left: 20px;'> <a href='index.php?controle=produtoController&acao=areaproduto&id=$id'>"
                        . " <span class='glyphicon' title='Adicionar Produto'> Adicionar + </span>"
                        . "</a> </td>";
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
