
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Aplicativo Safra</title>
        <!-- css -->
        <!-- Bootstrap core CSS -->
        <link href="../includes/css/bootstrap.min.css" rel="stylesheet">
        <!-- JQuery -->
        <script src="../includes/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../includes/jquery/jquery.maskMoney.js" type="text/javascript"></script>
        <script src="../includes/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="../includes/js/validarcampos.js" type="text/javascript"></script>

        <style type="text/css">
            .error{
                color: red;
                font-size: 12px;
            }
            body {
                min-height: 2000px;
                padding-top: 85px;
            }

            #menutitle{
                color: white;
            }
            #imagesituationcerto{
                background-image: url(../includes/imagens/certo.png);
                display: none;
                background-repeat: no-repeat;
            }
            #imagesituationerro{
                background-image: url(../includes/imagens/erro.png);
                display: none;
                background-repeat: no-repeat;
            }
            #mensagemlabelerro{
                display: none;
                color: red;
            }
            #mensagemredirecionamento{
                color: #006400;
                display: none;
            }
        </style>
        <script>
            jQuery(function ($) {
                $("#cpf").mask("999.999.999-99");
            });
        </script>
    </head>
<body>
    <div class="col-md-6 col-offset-2" style="margin-left: 25%; margin-right: 25%;">
        <div class="panel panel-primary" style="border-color: #006400">
            <div class="panel-heading" style="background-color: #006400">Cadastre-se</div>
            <?php 
                if($_GET != null){
                    if($_GET['mensagem'] == 'erro'){?>
                    <label style="margin-left: 15px; margin-top: 5px;">
                        <div class="alert alert-danger">
                            Não foi possível realizar o cadastro pois este CPF já existe em nosso banco de dados!
                        </div>
                    </label>
                    <?php }else if($_GET['mensagem'] == 'erro1'){?>
                    <label style="margin-left: 15px; margin-top: 5px;">
                        <div class="alert alert-danger">
                            Não foi possível realizar o cadastro, entre em contato com o administrador! Erro na base de dados :(
                        </div>
                    </label>
                    <?php }else if($_GET['mensagem'] == 'ok'){ ?>
                        <label>
                            <div class="alert alert-success">
                                Prezado, seu cadastro foi realizado com sucesso! Favor aguardar a liberação do Administrador.
                                <br/>
                                Estamos redirecionando você para a tela de login.
                                <img src="../includes/imagens/loading.gif" style="width: 50px;">
                            </div>
                            <?php header("refresh: 10;login.php"); ?>
                        </label>
                    <?php }
                }
            ?>
            <div class="panel-body">
                <form action="insereprimeirocadastro.php" name="formAlteracaosenha" id="formAlteracaosenha" method="POST" role="form">
                    <div class="row">
                        <div class="col-md-8">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="nome" required="" minlength="8" name="nome" placeholder="Digite o Nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <label for="cpf">CPF</label>
                            <input type="text" class="form-control" name="cpf" required="" id="cpf_atualizado" placeholder="Digite o CPF" 
                                   value="" onkeypress="return Onlynumbers(event)">
                        </div>
                    </div>
                    <script>
                        function verificarSenhas() {
                            if (document.formAlteracaosenha.senha.value == document.formAlteracaosenha.confirmasenha.value) {
                                $('#imagesituationerro').hide();
                                $('#mensagemlabelerro').hide();
                                $('#imagesituationcerto').show();
                                $('#definasenha').fadeOut(1000);
                            } else {
                                document.formAlteracaosenha.confirmasenha.focus();
                                $('#imagesituationcerto').hide();
                                $('#imagesituationerro').show();
                                $('#mensagemlabelerro').show();
                                $('#definasenha').fadeIn(1000);
                            }
                        };
                    </script>
                    <div class="row">
                        <div class="col-md-8">
                            <label for="senha">Nova Senha</label>
                            <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite a Senha" 
                                   required minlength="6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <label for="confirmasenha">Confirma Nova Senha</label>
                            <input type="password" class="form-control" id="confirmasenha" name="confirmasenha" placeholder="Digite novamente a Senha" required minlength="6" onchange="verificarSenhas()"  >
                        </div>
                        <div id="imagesituationcerto" class="col-md-1" style="padding-left: -0px; top: 28px; padding-right: 0px; height: 30px;">
                        </div>
                        <div id="imagesituationerro" class="col-md-1" style="padding-left: -0px; top: 28px; padding-right: 0px; height: 30px;">
                        </div>
                    </div>
                    <div id="mensagemlabelerro">
                        <label style="font-size: 12px">As senhas não conferem :( Por favor informe as  senhas novamente!</label>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-2">
                            <label for="tipousuario" style="margin-top: 3px;">Tipo Usuário:</label>
                        </div>
                        <input type="radio" name="tipousuario" value="P" checked="" title="Autônomo - Negócio Próprio"> Particular &nbsp;
                        <input type="radio" name="tipousuario" value="C" title="Trabalhador Empresarial"> Comercial
                    </div>
                    <br/>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <a href="login.php" class="btn btn-success" role="button">Voltar</a>
                </form>
            </div>
        </div>
    </div>
    <script src="../includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="../includes/js/jquery.validate.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $("#formAlteracaosenha").validate({
                rules: {
                    nome: {
                        required: true,
                        minlength: 8,
                        maxlength: 40
                    },
                    cpf: {
                        required: true
                    },
                    senha: {
                        required: true,
                        minlength: 6,
                        maxlength: 20
                    },
                    confirmasenha: {
                        required: true,
                        minlength: 6,
                        maxlength: 20
                    }
                },
                messages: {
                    nome: {
                        required: "Por favor, informe o Nome",
                        minlength: "O Nome deve ter pelo menos 8 caracteres",
                        maxlength: "O Nome deve ter no máximo 40 caracteres"
                    },
                    cpf: {
                        required: "Por favor, informe o CPF"
                    },
                    senha: {
                        required: "Por favor, informe a Nova Senha",
                        minlength: "A Nova Senha deve ter pelo menos 6 caracteres",
                        maxlength: "A Nova Senha deve ter no máximo 20 caracteres"
                    },
                    confirmasenha: {
                        required: "Por favor, informe o campo Confirma Nova Senha",
                        minlength: "O Campo Confirma Nova Senha deve ter pelo menos 6 caracteres",
                        maxlength: "O Campo Confirma Nova Senha deve ter no máximo 20 caracteres"
                    }
                }
            })
            $('#btn').click(function () {
                $("#formAlteracaosenha").valid();
            });
        });
    </script>
</body>
<!-- Recebe os posts -->
</html>
