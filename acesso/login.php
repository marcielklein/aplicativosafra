
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Aplicativo Safra</title>
        <!-- css -->
        <!-- Bootstrap core CSS -->
        <link href="../includes/css/bootstrap.min.css" rel="stylesheet">
        <!-- JQuery -->
        <script src="../includes/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../includes/jquery/jquery.maskMoney.js" type="text/javascript"></script>
        <script src="../includes/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="../includes/includes/js/validarcampos.js" type="text/javascript"></script>
        <style type="text/css">
            
            body {padding-top: 70px; background-color: #f0f0f0; color: #313131; line-height: 1; padding: 5px;}
            .form-login {max-width: 450px; max-height: 350px; margin-left: auto; margin-right: auto; padding: 10px; border-radius: 10px; margin-top: 10%;}
            .btn {display: inline-block; padding: 6px 12px; border: 2px solid #ddd; color: #ddd; text-decoration: none; font-weight: 700; text-transform: uppercase;
                  margin: 15px 5px 0; transition: all .2s; min-width: 100px; text-align: center; font-size: 14px;}
            .btn.primary {border-color: #006400; color: #006400;}
            #formlogin .mensagemerro{color: red; height: 34px;}
            .error{color: red; font-size: 12px;}
            a:link {text-decoration:none; }
        </style>
        <script>
            jQuery(function ($) {
                $("#cpf").mask("999.999.999-99");
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="form-login">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:  #006400;">
                        <div class="panel-title" style="color: white">Login do Sistema</div>
                    </div>
                    <div style="padding-top:5px" class="panel-body">
                        <div style="display:none" id="result" class="alert alert-danger col-sm-12">
                        </div>
                        <form  action="ope.php" method="post" id="formlogin" name="formLogin" class="form" role="form">
                            <h2 class="form-signin-heading">Login</h2>
                            <label for="cpf" class="sr-only">CPF</label>
                                        <input type="cpf" id="cpf" name="cpf" id="cpf_atualizado" class="form-control" placeholder="Informe seu CPF">
                            <br/>
                            <label for="inputPassword" class="sr-only">Senha</label>
                            <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="lembrar-me"> Lembrar-me
                              </label>
                            </div>
                            <div class="row">
	                            <div class="form-group">
	                                <div class="col-sm-12 controls">
	                                    <input type="submit" value="Acessar" class="btn primary btn" />
                                            <a href="formPrimerocadastro.php" class="btn btn-success" role="button">Cadastre-se</a>
	                                </div>
	                            </div>
	                        </div>

                        </form>
                    </div>
                </div>
            </div>
            <script src="../includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
            <script src="../includes/js/jquery.validate.min.js" type="text/javascript"></script>
            
            <script>
                $("#formlogin").validate({
                    rules: {
                        cpf: {
                            required: true,
                            minlength: 14
                        },
                        senha: {
                            required: true,
                            minlength: 8
                        }
                    },
                    messages: {
                        cpf: {
                            required: "Por favor, informe o CPF do Usuário",
                            minlength: "O Login deve ter pelo menos 14 caracteres"
                        },
                        senha: {
                            required: "Por favor, informe a Senha do Usuário",
                            minlength: "A Senha deve ter pelo menos 8 caracteres"
                        }
                    }
                });
            </script>
    </body>
</html>